//const SendOtp = require('sendotp');
//const sendOtp = new SendOtp('AuthKey');
const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const Expense = mongoose.model('Expense');
const ExpenseLine = mongoose.model('ExpenseLine');
const Currency = mongoose.model('Currency');
const Employee = mongoose.model('employee');
const ExpenseLineStatus = mongoose.model('ExpenseLineStatus');
const ExpenseStatus = mongoose.model('ExpenseStatus');
const ExpenseType = mongoose.model('ExpenseType');
const Manager = mongoose.model('Manager');
const CostCenter = mongoose.model('CostCenter');
const Location = mongoose.model('Location');
const Password = mongoose.model('Password');
var dateFormat = require('dateformat');
var nodemailer = require('nodemailer');
var MongoClient = require('mongodb').MongoClient;
let mongoXlsx = require('mongo-xlsx');
var moment = require('moment');
const AWS = require('aws-sdk');
let fs = require('fs');
const logger = require('./config/logger')
var jwt_decode = require('jwt-decode');
var mongodb = require('mongodb');
const multer = require('multer');
const path = require('path');
const aeskey = "THisisMyKeyTHisM";
var ObjectID = mongodb.ObjectID;
var async = require('async');
var randomstring = require('randomstring');
var bodyParser = require('body-parser');
var request = require('request');
var app = express();
var crypto = require('crypto');
const algorithm = 'aes-256-cbc';
const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var url = "mongodb://localhost:27017/";

router.post('/report', async (req, res, next) => {
    try {    //router for report
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        if (doc.employeeRole == "Admin") {
                                            var promise = job1();
                                            promise.then(function (data1) {
                                                console.log("method1");
                                                logger.log('info', "method");
                                                return job2(data1);
                                            })
                                                .then(function (data2) {
                                                    console.log("method2");
                                                    logger.log('info', "method2");
                                                    //console.log('data2',JSON.stringify( data2));
                                                    //  console.log("latest report.............................++++++"+JSON.stringify(data2))
                                                    return job3(data2);
                                                }).then(function (data3) {
                                                    console.log("latest report................ " + JSON.stringify(data3))
                                                    console.log("method3");
                                                    logger.log('info', "method3");
                                                    try {
                                                        console.log("valure pf data3=>" + data3.length);
                                                        logger.log('info', ("valure of data3=>" + data3.length));
                                                        if ((data3).length == 0) {
                                                            var data = null;
                                                            var status = false;
                                                            var message = "Record Not found";
                                                            logger.log('info', "record not found");
                                                            var err = "null";
                                                            var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                            res.json(responseFormatDict);
                                                            logger.log('info', responseFormatDict);
                                                        }
                                                        else {
                                                            var xl = require('excel4node');
                                                            //console.log('hhh'+data3.length);
                                                            logger.log('info', ("length=>" + data3.length));
                                                            // Create a new instance of a Workbook class
                                                            var wb = new xl.Workbook();
                                                            // Add Worksheets to the workbook
                                                            var ws = wb.addWorksheet('Sheet 1');
                                                            var ws2 = wb.addWorksheet('Sheet 2');
                                                            // Create a reusable style
                                                            var style = wb.createStyle({
                                                                font: {
                                                                    color: '#080707',
                                                                    size: 12,
                                                                },
                                                                //numberFormat: '$#,##0.00; ($#,##0.00); -',
                                                            });
                                                            var style1 = wb.createStyle({
                                                                font: {
                                                                    color: '#080707',
                                                                    size: 13,
                                                                    bold: true
                                                                },
                                                                //numberFormat: '$#,##0.00; ($#,##0.00); -',
                                                            });
                                                            // Set value of cell A1 to 100 as a number type styled with paramaters of style
                                                            ws.cell(1, 1)
                                                                .string('ExpenseID')
                                                                .style(style1);
                                                            // Set value of cell B1 to 200 as a number type styled with paramaters of style
                                                            ws.cell(1, 2)
                                                                .string('EmployeeID')
                                                                .style(style1);
                                                            ws.cell(1, 3)
                                                                .string('EmployeeName')
                                                                .style(style1);
                                                            ws.cell(1, 4)
                                                                .string('ManagerName')
                                                                .style(style1);
                                                            ws.cell(1, 5)
                                                                .string('ExpensePurpose')
                                                                .style(style1);
                                                            ws.cell(1, 6)
                                                                .string('Submitted Date')
                                                                .style(style1);
                                                            ws.cell(1, 7)
                                                                .string('Approved Date')
                                                                .style(style1);
                                                            ws.cell(1, 8)
                                                                .string('CostCenter')
                                                                .style(style1);
                                                            ws.cell(1, 9)
                                                                .string('ExpenseType')
                                                                .style(style1);
                                                            ws.cell(1, 10)
                                                                .string('Merchant')
                                                                .style(style1);
                                                            ws.cell(1, 11)
                                                                .string('Amount')
                                                                .style(style1);
                                                            ws.cell(1, 12)
                                                                .string('No of employees')
                                                                .style(style1);
                                                            ws.cell(1, 13)
                                                                .string('Name of employees')
                                                                .style(style1);
                                                            ws.cell(1, 14)
                                                                .string('Currency')
                                                                .style(style1);
                                                            ws.cell(1, 15)
                                                                .string('Location')
                                                                .style(style1);
                                                            ws.cell(1, 16)
                                                                .string('ReceiptID')
                                                                .style(style1);
                                                            ws.cell(1, 17)
                                                                .string('ReceiptLink')
                                                                .style(style1);
                                                            ws.cell(1, 18)
                                                                .string('Comments')
                                                                .style(style1);
                                                            ws.cell(1, 19)
                                                                .string('Status')
                                                                .style(style1);
                                                            // Set value of cell C1 to a formula styled with paramaters of style
                                                            // ws.cell(1, 3)
                                                            //.formula('A1 + B1')
                                                            // .style(style);
                                                            // var j=0;
                                                            var m = 0;
                                                            var newLen = 0;
                                                            while (m != data3.length) {
                                                                newLen = newLen + (data3[m].expenses).length;
                                                                m = m + 1;
                                                            }
                                                            console.log("length is=>" + newLen);
                                                            logger.log('info', ("length is=>" + newLen));
                                                            var lengthOfExpenses;
                                                            console.log("=>" + data3.length);
                                                            logger.log('info', data3.length);
                                                            console.log("expenses=>" + (data3[0].expenses).length);
                                                            logger.log('info', (data3[0].expenses).length);
                                                            var j;
                                                            var p = 0;
                                                            for (j = 0; j < (newLen + 1); j++) {
                                                                try {
                                                                    console.log("han" + data3.length);
                                                                    logger.log('info', data3.length);
                                                                    // var i=0;
                                                                    // Set value of cell A2 to 'string' styled with paramaters of style
                                                                    if (p < data3.length) {
                                                                        ws.cell(j + 2, 1)
                                                                            .string(data3[p].expenseID)
                                                                            .style(style);
                                                                        ws.cell(j + 2, 2)
                                                                            .string(data3[p].employeeID)
                                                                            .style(style);
                                                                        ws.cell(j + 2, 3)
                                                                            .string(data3[p].employeeName)
                                                                            .style(style);
                                                                        ws.cell(j + 2, 4)
                                                                            .string(data3[p].managerReferenceID)
                                                                            .style(style);
                                                                        ws.cell(j + 2, 5)
                                                                            .string(data3[p].expensePurpose)
                                                                            .style(style);
                                                                        ws.cell(j + 2, 6)
                                                                            .string(data3[p].updatedDate)
                                                                            .style(style);
                                                                        ws.cell(j + 2, 7)
                                                                            .string(data3[p].approvedDate)
                                                                            .style(style);
                                                                        var len = ((data3[p].expenses).length);
                                                                        //p=p+1;
                                                                        //console.log("222=>"+data3[p].expenses[j].merchant);
                                                                        var len = ((data3[p].expenses).length);
                                                                        console.log("lengtyh of expeneses" + len);
                                                                        logger.log('info', ("length of expenses" + len));
                                                                        var i = 0;
                                                                        var w = j;
                                                                        for (var q = 0; q < len; q++) {
                                                                            try {
                                                                                console.log("j,q" + q);
                                                                                logger.log('info', q);
                                                                                console.log("data3" + data3[p].expenses[q].expenseType)
                                                                                ws.cell(w + 2, 8)
                                                                                    .string(data3[p].expenses[q].costCenter)
                                                                                    .style(style);
                                                                                console.log("hgg  1")
                                                                                ws.cell(w + 2, 9)
                                                                                    .string(data3[p].expenses[q].expenseType)
                                                                                    .style(style);
                                                                                console.log("hgg  2")
                                                                                ws.cell(w + 2, 10)
                                                                                    .string(data3[p].expenses[q].merchant)
                                                                                    .style(style);
                                                                                console.log("hgg  3")
                                                                                ws.cell(w + 2, 11)
                                                                                    .string(data3[p].expenses[q].amount.toString())
                                                                                    .style(style);
                                                                                console.log("hgg  4")
                                                                                ws.cell(w + 2, 12)
                                                                                    .string(data3[p].expenses[q].noOfEmployee.toString())
                                                                                    .style(style);
                                                                                console.log("hgg  5")
                                                                                ws.cell(w + 2, 13)
                                                                                    .string(data3[p].expenses[q].nameOfEmployee)
                                                                                    .style(style);
                                                                                console.log("hgg  6")
                                                                                ws.cell(w + 2, 14)
                                                                                    .string(data3[p].expenses[q].currency)
                                                                                    // console.log("7"+data3[p].expenses[q].receiptURL)
                                                                                    .style(style);
                                                                                //console.log("hgg  8")
                                                                                ws.cell(w + 2, 15)
                                                                                    .string(data3[p].expenses[q].location)
                                                                                    .style(style);
                                                                                //  console.log("2")
                                                                                console.log("hgg  9")
                                                                                ws.cell(w + 2, 16)
                                                                                    .string(data3[p].expenses[q].receiptID)
                                                                                    .style(style);
                                                                                // console.log("hgg  10")
                                                                                ws.cell(w + 2, 17)
                                                                                    // console.log("hgg  11")
                                                                                    .string(data3[p].expenses[q].receiptURL)
                                                                                    //  console.log("url"+data3[p].expenses[q].receiptURL)
                                                                                    .style(style);
                                                                                // console.log("hgg  12")
                                                                                ws.cell(w + 2, 18)
                                                                                    .string(data3[p].expenses[q].comments)
                                                                                    .style(style);
                                                                                // console.log("hgg  13")
                                                                                ws.cell(w + 2, 19)
                                                                                    .string("Approved")
                                                                                    .style(style);
                                                                                // console.log("i,w"+i,w)
                                                                                //  console.log("gh")
                                                                                i = i + 1;
                                                                                w = w + 1;
                                                                                console.log("1111111111111111111=>" + j);
                                                                                logger.log('info', j);
                                                                            }
                                                                            catch (e) {
                                                                                console.log("ividano" + e)
                                                                                catchFormat(e, res);
                                                                            }
                                                                        }
                                                                        j = (j + ((data3[p].expenses).length)) - 1;
                                                                        console.log("valuev of j" + j);
                                                                        logger.log('info', ("valuev of j" + j));
                                                                        console.log("valuev of p" + p);
                                                                        logger.log('info', ("valuev of p" + p));
                                                                        p = p + 1;
                                                                    }
                                                                    else {
                                                                        console.log("exceed");
                                                                    }
                                                                }
                                                                catch (e) {
                                                                    catchFormat(e, res);
                                                                }
                                                            }
                                                            // Set value of cell A3 to true as a boolean type styled with paramaters of style but with an adjustment to the font size.
                                                            /* ws.cell(3, 1)
                                                            .bool(true)
                                                            .style(style)
                                                            .style({font: {size: 14}});*/
                                                            var dateOFCurrent = Date.now();
                                                            var fileName = dateOFCurrent + 'Excel.xlsx';
                                                            console.log("check reportt")
                                                            wb.write(__dirname + "/Reports/" + fileName);
                                                            //fileName= "1584521794736Excel.xlsx";
                                                            //console.log("eeeee");
                                                            function getFilesizeInBytes(filename) {
                                                                const stats = fs.statSync(filename);
                                                                const fileSizeInBytes = stats.size;
                                                                return fileSizeInBytes;
                                                            }
                                                            // var p=getFilesizeInBytes(fileName);
                                                            // console.log("size=>"+p);
                                                            return fileName;
                                                        }
                                                    }
                                                    catch (e) {
                                                        catchFormat(e, res);
                                                    }
                                                }).then(function (fileName) {
                                                    // console.log("method4");
                                                    // var onedrive_folder ='ERTAppFolder';
                                                    // var onedrive_subfolder = 'Reports';
                                                    setTimeout(function () {
                                                        fs.access(__dirname + "/Reports/" + fileName, (err) => {
                                                            if (err) {
                                                                console.log("The file does not exist.");
                                                                console.log(__dirname)
                                                                console.log(err)
                                                                logger.log('info', "The file does not exist.");
                                                                logger.log('info', __dirname)
                                                                logger.log('info', err)
                                                            } else {
                                                                console.log("The file exists.");
                                                                console.log("filepath" + __dirname + "/Reports/" + fileName);
                                                                logger.log('info', "The file exists.");
                                                                logger.log('info', (__dirname + "/Reports/" + fileName));
                                                                fs.readFile(__dirname + "/Reports/" + fileName, function read(e, f) {
                                                                    // var userEmail=req.body.username;
                                                                    //console.log("fs read"+ userEmail);
                                                                    //logger.log('info',userEmail);
                                                                    try {
                                                                        const ID = 'AKIA26P6GBEW7ZTCJI4S';
                                                                        const SECRET = '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';
                                                                        // The name of the bucket that you have created
                                                                        const BUCKET_NAME = 'gxxstorage';
                                                                        const s3 = new AWS.S3({
                                                                            accessKeyId: ID,
                                                                            secretAccessKey: SECRET
                                                                        });
                                                                        let ts = Date.now();
                                                                        // timestamp in milliseconds
                                                                        console.log(ts);
                                                                        // timestamp in seconds
                                                                        let namePdf = Math.floor(ts / 1000);
                                                                        let name = namePdf + '.xlsx';
                                                                        const uploadFile = (fileName) => {
                                                                            // Read content from the file
                                                                            const fileContent = fs.readFileSync(fileName);
                                                                            // Setting up S3 upload parameters
                                                                            var folder = 'Reports';
                                                                            const params = {
                                                                                Bucket: BUCKET_NAME,
                                                                                //Key: name, // File name you want to save as in S3
                                                                                Key: `${folder}/${name}`,
                                                                                // ServerSideEncryption: "AES256",
                                                                                ContentDisposition: "inline",
                                                                                ContentType: 'application/msexcel',
                                                                                ACL: 'public-read',
                                                                                Body: fileContent
                                                                            };
                                                                            /* const getUrlFromBucket=(s3Bucket,fileName)=>{
                                                                                    return 'https://'+s3Bucket.config.params.Bucket+'.s3-'
                                                                                            + s3Bucket.config.region+'.amazonaws.com/'+fileName
                                                                                };
                                                                                var u=getUrlFromBucket('gxxstorage',name);
                                                                                console.log('The URL ishh', u);*/
                                                                            // Uploading files to the bucket
                                                                            s3.upload(params, function (err, data) {
                                                                                if (err) {
                                                                                    throw err;
                                                                                }
                                                                                // var params = {Bucket: 'gxxstorage', Key: '${folder}/${name}'};
                                                                                s3.getSignedUrl('putObject', params, function (err, signedURL) {
                                                                                    console.log('The URL is', signedURL);
                                                                                    console.log('The URL is...', signedURL.split("?")[0]);
                                                                                    /*var s3url = s3.getSignedUrl('getObject', {Key: params.Key});
                                                                                    console.log('The is', s3url);*/
                                                                                    const myBucket = 'gxxstorage'
                                                                                    const myKey = 'NCqjsg0ZDWf3BeZeQIuZNBNqAJI+FEoaGMd/SpSG'
                                                                                    const signedUrlExpireSeconds = 60 * 5
                                                                                    const url = s3.getSignedUrl('getObject', {
                                                                                        Bucket: myBucket,
                                                                                        Key: myKey,
                                                                                        Expires: signedUrlExpireSeconds
                                                                                    })
                                                                                    // console.log(url)
                                                                                    var data = { url: signedURL.split("?")[0] };
                                                                                    //   var msg1="Hi Admin"+'<br>'+"Access the expense report from "+req.body.fromDate+' to '+req.body.toDate+'.'+"<br>To view click here: "+'<a href='+signedURL.split("?")[0]+'>Click here </a><br>'+ " To download click here: "+'<a href='+signedURL.split("?")[0]+'>Click here</a>'+'<br><br><br>Regards,';
                                                                                    var msg1 = "Hi Admin" + '<br>' + "Access the expense report from " + req.body.fromDate + ' to ' + req.body.toDate + '.' + '<br><a href=' + signedURL.split("?")[0] + '>Click here </a>' + " to download" + '<br><br><br>Regards,';
                                                                                    emailNotification('hr@g10x.com', msg1, 'GXP-Report');
                                                                                    var status = true;
                                                                                    var message = "Report generated";
                                                                                    console.log(message);
                                                                                    logger.log('info', message);
                                                                                    var error = null;
                                                                                    var callresponse = responseFormat(data, message, status, error);
                                                                                    res.send(callresponse);
                                                                                    logger.log('info', callresponse);
                                                                                    // console.log(`File uploaded successfully. ${data.Location}`);
                                                                                });
                                                                            })
                                                                        };
                                                                        uploadFile(__dirname + "/Reports/" + fileName);
                                                                        clearInterval(function () { });
                                                                        clearInterval();
                                                                    }
                                                                    catch (e) {
                                                                        catchFormat(e, res);
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }, 5000);
                                                })
                                            function job1() {
                                                try {
                                                    if (req.body.fromDate != null) {
                                                        if ((req.body.fromDate).length != 0) {
                                                            if (req.body.toDate != null) {
                                                                if ((req.body.toDate).length != 0) {
                                                                    if (req.body.username != null) {
                                                                        if ((req.body.username).length != 0) {
                                                                            console.log("asdsasaadsdaads")
                                                                            return new Promise(function (resolve, reject) {
                                                                                date = new Date(req.body.fromDate).setHours(0, 0, 0, 0)
                                                                                endDate = new Date(req.body.toDate).setHours(23, 59, 59, 59)
                                                                                var item = [];
                                                                                var mysort = { approvedDate : -1 };
                                                                                Expense.find({
                                                                                    $and: [{
                                                                                        approvedDate : {
                                                                                            $gte: date,
                                                                                            $lte: endDate
                                                                                        }
                                                                                    }, { expenseStatusID: 5 }]
                                                                                }, { '__v': 0 }, { sort: mysort })

                                                                                    .then(function (result) {
                                                                                        console.log("report is "+result);
                                                                                        result.forEach(element => {
                                                                                            var dateObject = dateFormat(new Date(element.updatedDate), "mm-dd-yyyy");
                                                                                            var dateObject1 = dateFormat(new Date(element.approvedDate), "mm-dd-yyyy");
                                                                                            var testData;
                                                                                            Manager.findOne({ _id: element.managerReferenceID }, (err, doc6) => {
                                                                                                try {
                                                                                                    // console.log(doc6)
                                                                                                    if(doc6){
                                                                                                        testData = {
                                                                                                            id: element._id,
                                                                                                            employeeID: element.employeeID,
                                                                                                            employeeName: element.employeeName,
                                                                                                            expenseID: element.expenseID,
                                                                                                            managerReferenceID: doc6.firstName + " " + doc6.lastName,
                                                                                                            expensePurpose: element.expensePurpose,
                                                                                                            expenseStatusID: element.expenseStatusID,
                                                                                                            updatedDate: dateObject,
                                                                                                            approvedDate: dateObject1,
                                                                                                            expenses: []
                                                                                                        }
                                                                                                        item.push(testData);
                                                                                                    }else{
                                                                                                        // console.log(doc6,"emptyArrr")
                                                                                                        testData = {
                                                                                                            id: element._id,
                                                                                                            employeeID: element.employeeID,
                                                                                                            employeeName: element.employeeName,
                                                                                                            expenseID: element.expenseID,
                                                                                                            managerReferenceID: "N/A",
                                                                                                            expensePurpose: element.expensePurpose,
                                                                                                            expenseStatusID: element.expenseStatusID,
                                                                                                            updatedDate: dateObject,
                                                                                                            approvedDate: dateObject1,
                                                                                                            expenses: []
                                                                                                        }
                                                                                                        // console.log(testData,"shouldbenull")
                                                                                                        item.push(testData);
                                                                                                    }
                                                                                                }
                                                                                                catch (e) {
                                                                                                    console.log(err,"sssssssssssssss")
                                                                                                    catchFormat(e, res);
                                                                                                }
                                                                                            });
                                                                                        });
                                                                                        console.log("item=>");
                                                                                        //return item;
                                                                                    })
                                                                                setTimeout(function () {
                                                                                    resolve(item);
                                                                                }, 5000);
                                                                            });
                                                                        }
                                                                        else {
                                                                            var data = null;
                                                                            var status = false;
                                                                            var message = "username is required";
                                                                            var err = err;
                                                                            var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                            res.json(responseFormatDict);    //response
                                                                            console.log('Error during record insertion : ' + err);
                                                                            logger.log('info', ('Error during record insertion : ' + err));
                                                                        }
                                                                    }
                                                                    else {
                                                                        var data = null;
                                                                        var status = false;
                                                                        var message = "username field is required";
                                                                        var err = err;
                                                                        var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                        res.json(responseFormatDict);    //response
                                                                        logger.log('info', responseFormatDict);
                                                                        console.log('Error during record insertion : ' + err);
                                                                        logger.log('info', ('Error during record insertion : ' + err));
                                                                    }
                                                                }
                                                                else {
                                                                    var data = null;
                                                                    var status = false;
                                                                    var message = "toDate is required";
                                                                    var err = err;
                                                                    var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                    res.json(responseFormatDict);    //response
                                                                    logger.log('info', responseFormatDict);
                                                                    console.log('Error during record insertion : ' + err);
                                                                    logger.log('info', ('Error during record insertion : ' + err));
                                                                }
                                                            }
                                                            else {
                                                                var data = null;
                                                                var status = false;
                                                                var message = "toDate field is required";
                                                                var err = err;
                                                                var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                res.json(responseFormatDict);    //response
                                                                logger.log('info', responseFormatDict);
                                                                console.log('Error during record insertion : ' + err);
                                                                logger.log('info', ('Error during record insertion : ' + err));
                                                            }
                                                        }
                                                        else {
                                                            var data = null;
                                                            var status = false;
                                                            var message = "fromDate is required";
                                                            var err = err;
                                                            var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                            res.json(responseFormatDict);    //response
                                                            logger.log('info', responseFormatDict);
                                                            console.log('Error during record insertion : ' + err);
                                                            logger.log('info', ('Error during record insertion : ' + err));
                                                        }
                                                    }
                                                    else {
                                                        var data = null;
                                                        var status = false;
                                                        var message = "fromDate is required";
                                                        var err = err;
                                                        var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                        res.json(responseFormatDict);    //response
                                                        logger.log('info', responseFormatDict);
                                                        console.log('Error during record insertion : ' + err);
                                                        logger.log('info', ('Error during record insertion : ' + err));
                                                    }
                                                }
                                                catch (e) {
                                                    catchFormat(e, res);
                                                }
                                            }
                                            function job2(item) {
                                                return new Promise(function (resolve, reject) {
                                                    var resultdata = [];
                                                    item.forEach(element1 => {
                                                        // console.log("element1=>"+JSON.stringify(element1.id));
                                                        ExpenseLine.find({ expenseID: element1.id }, { createdDate: 0, updatedDate: 0 }, (err, docs) => {
                                                            if (!err) {
                                                                try {
                                                                    // console.log("item=>"+docs);
                                                                    docs.forEach(element2 => {
                                                                        Currency.findOne({ ID: element2.currency }, (err, doc8) => {
                                                                            try {
                                                                                ExpenseType.findOne({ ID: element2.expenseType }, (err, doc9) => {
                                                                                    //console.log("expenseType"+element2.expenseType);
                                                                                    var exType;
                                                                                    if (element2.expenseType == 6) {
                                                                                        exType = element2.otherExpense;
                                                                                    }
                                                                                    else {
                                                                                        exType = doc9.expenseType;
                                                                                    }
                                                                                    //console.log("other ex"+element2.otherExpense);
                                                                                    //console.log("exType....=>"+exType)
                                                                                    try {
                                                                                        var testData = {
                                                                                            id: element2._id,
                                                                                            currency: doc8.currency,
                                                                                            expenseType: exType,
                                                                                            merchant: element2.merchant,
                                                                                            costCenter: element2.costCenter,
                                                                                            noOfEmployee: element2.noOfEmployee,
                                                                                            nameOfEmployee: element2.nameOfEmployee,
                                                                                            location: element2.location,
                                                                                            amount: element2.amount,
                                                                                            comments: element2.comments,
                                                                                            receiptID: element2.receiptID,
                                                                                            receiptURL: element2.url,
                                                                                            webURL: element2.webURL,
                                                                                            Date: element2.Date
                                                                                        }
                                                                                        element1.expenses.push(testData);
                                                                                    }
                                                                                    catch (e) {
                                                                                        catchFormat(e, res);
                                                                                    }
                                                                                })
                                                                            }
                                                                            catch (e) {
                                                                                catchFormat(e, res);
                                                                            }
                                                                        })
                                                                    });
                                                                    resultdata.push(element1);
                                                                }
                                                                catch (e) {
                                                                    catchFormat(e, res);
                                                                }
                                                            }
                                                            // console.log("result"+JSON.stringify( resultdata));
                                                        })
                                                    })
                                                    setTimeout(function () {
                                                        resolve(resultdata);
                                                    }, 1000);
                                                });
                                            }
                                            function job3(data2) {
                                                return new Promise(function (resolve, reject) {
                                                    setTimeout(function () {
                                                        resolve(data2);
                                                    }, 1000);
                                                });
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "No Access";
                                            var err = "null";
                                            var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
});
router.post('/inProgressStatusUpdation', (req, res) => {     //router for improgreessStatusUpdation
    InProgressUpdation(req, res);
});

router.post('/deleteExpenseLine', (req, res) => {     //router for save Expense
    DeleteExpenseLine(req, res);
});

router.post('/saveExpense', (req, res) => {     //router for save Expense
    saveExpense(req, res);
});

router.post('/getLocations', (req, res) => {     //router for save Expense
    getLocation(req, res);
});

router.post('/getCostcenters', (req, res) => {     //router for save Expense
    getCostcenter(req, res);
});

router.post('/saveExpenseLine', (req, res) => {   //router for save ExpenseLine
    saveExpenseLine(req, res);
});

router.post('/getAllCurrencies', (req, res) => {   //router for save ExpenseLine
    getCurrency(req, res);
});

router.post('/resetPassword', (req, res) => {   //router for save ExpenseLine
    resetPassword(req, res);
});

router.post('/getExpenseTypes', (req, res) => {   //router for save ExpenseLine
    getExpenseType(req, res);
});

router.post('/search', (req, res) => {             //search for expense details in date range
    searchByDaterange(req, res);
});

router.post('/logout', (req, res) => {             //search for expense details in date range
    logout(req, res);
});

router.post('/deleteExpense', (req, res) => {             //search for expense details in date range
    deleteExpense(req, res);
});

router.post('/getManagers', (req, res) => {             //search for expense details in date range
    getManagers(req, res);
});

router.post('/getExpenseLine', (req, res) => {             //search for expense details in date range
    getExpenseLine(req, res);
});

router.post('/getExpenseLineDetails', (req, res) => {             //search for expense details in date range
    getExpenseLineDetails(req, res);
});

router.post('/retrieveExpensesApprovedByManager', (req, res) => {             //search for expense details in date range ///verify and pay
    verifyAndPay(req, res);
});

router.post('/resendOTP', (req, res) => {             //search for expense details in date range ///verify and pay
    resendOTP(req, res);
});

router.post('/viewAllOpenRequests', (req, res) => {             //search for expense details in date range
    //console.log(req.body);
    ViewOpenRequest(req, res);
});

router.post('/declineComments', (req, res) => {             //search for expense details in date range
    console.log(req.body);
    DeclineComment(req, res);
});

router.post('/expenseStatusUpdation', (req, res) => {             //search for expense details in date range
    console.log(req.body);
    ExpenseStatusUpdation(req, res);
});

router.post('/getManagerReferenceID', (req, res) => {             //search for expense details in date range
    console.log(req.body);
    getManagerReferenceID(req, res);
});

router.post('/signUp', (req, res) => {             //search for expense details in date range
    console.log(req.body);
    signUp(req, res);
});

router.post('/searchBYExpenseID', (req, res) => {             //search for expense details in date range
    searchByExpense(req, res);
});

router.post('/retrieveExpensesByManagerId', (req, res) => {             //search for expense details in date range// approve request
    ApproveExpense(req, res);
});

router.post('/submitExpense', (req, res) => {             //search for expense details in date range
    SubmitExpense(req, res);
});

router.post('/acceptOrDecline', (req, res) => {             //search for expense details in date range
    AcceptOrDecline(req, res);
});

router.post('/verifyOTP', (req, res) => {             //search for expense details in date range
    verifyOTP(req, res);
});

function catchFormat(e, res) {
    var data = null;
    var status = false;
    var message = "Something went wrong";
    console.log(message);
    logger.log('info', message);
    var err = e;
    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
    res.json(responseFormatDict);
    logger.log('info', responseFormatDict);
    logger.log('info', e);
}
function Add(req, res) {    //function for saveExpense to DB
    var expense = new Currency();
    var currdatetime = Date.now();
    expense.ID = req.body.ID;
    expense.currency = req.body.currency;
    expense.currencyCode = req.body.currencyCode;
    expense.currencyDescription = req.body.currencyDescription;
    expense.save((err, doc) => {    //saving datas
        if (!err) {                  // if there is no error ....
            console.log('Inserted succeesfylly: ', doc._id);
            logger.log('info', ('Inserted succeesfylly: ', doc._id));
            var data = { doc };
            var status = "true";
            var message = "inserted successfully";
            var err = "null";
            var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
            res.json(responseFormatDict);          // response
            logger.log('info', responseFormatDict);
        }
        else {                      // if there is error.....
            var data = null;
            var status = "false";
            var message = "err";
            var err = err;
            logger.log('info', responseFormatDict);
            console.log('Error during record insertion : ' + err);
            logger.log('info', err);
        }
    });
}
function responseFormat(data, message, status, err) {   //function for response format
    var responseFormatDict;
    responseFormatDict = { "data": data, "message": message, "status": status, "error": err };
    //console.log(",,,,"+JSON.stringify( responseFormatDict[0].data));
    return responseFormatDict;
}
function saveExpense(req, res) {    //function for saveExpense to DB
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        var currdatetime = Date.now();
                                        if ((typeof (req.body.managerReferenceID) != 'undefined') || (req.body.managerReferenceID != null)) {
                                            if ((req.body.managerReferenceID).length != 0) {
                                                if ((typeof (req.body.expensePurpose) != 'undefined') || (req.body.expensePurpose != null)) {
                                                    if ((req.body.expensePurpose).length != 0) {
                                                        if ((typeof (req.body.employeeName) != 'undefined') || (req.body.employeeName != null)) {
                                                            if ((req.body.employeeName).length != 0) {
                                                                if ((typeof (req.body.expenseID) != 'undefined') || (req.body.expenseID != null)) {
                                                                    if ((req.body._id == null) || (req.body._id == "")) {
                                                                        // var saveExpense=req.body.expenseStatusID;
                                                                        ExpenseStatus.findOne({ expenseStatus: "NotSubmitted" }, { _id: 0, ExpenseLineStatus: 0 }, (err, docs1) => {
                                                                            try {
                                                                                if (!err) {
                                                                                    Manager.findOne({ _id: req.body.managerReferenceID }, (err, docs) => {///find the manager
                                                                                        try {
                                                                                            if (docs == null) {
                                                                                                var data = null;
                                                                                                var status = false;
                                                                                                var message = "Manager is invalid";
                                                                                                var err = err;
                                                                                                var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                                                res.json(responseFormatDict);    //response
                                                                                                logger.log('info', responseFormatDict);
                                                                                                logger.log('info', err);
                                                                                            }
                                                                                            else {
                                                                                                var expense = new Expense();
                                                                                                var currdatetime = Date.now();  // get the currentdate
                                                                                                expense.employeeID = req.body.employeeID;
                                                                                                expense.employeeName = req.body.employeeName;
                                                                                                expense.expenseID = req.body.expenseID;
                                                                                                expense.managerReferenceID = req.body.managerReferenceID;
                                                                                                expense.expensePurpose = req.body.expensePurpose;
                                                                                                expense.expenseStatusID = docs1.ID;
                                                                                                expense.updatedDate = currdatetime;
                                                                                                expense.createdDate = currdatetime;
                                                                                                expense.reSubmittedToManager = false;
                                                                                                expense.reSubmittedToAdmin = false;
                                                                                                expense.deletionStatus = false;
                                                                                                expense.updatedBy = req.body.updatedBy;
                                                                                                expense.createdBy = req.body.createdBy;
                                                                                                console.log(expense.employeeID);
                                                                                                logger.log('info', expense.employeeID);
                                                                                                expense.save((err, doc) => {    //saving datas
                                                                                                    try {
                                                                                                        if (!err) {                  // if there is no error ....
                                                                                                            console.log('Inserted succeesfylly: ', doc._id);
                                                                                                            logger.log('info', ('Inserted succeesfylly: ', doc._id));
                                                                                                            var data = { expenseObjectId: doc._id };
                                                                                                            var status = true;
                                                                                                            var message = "inserted successfully";
                                                                                                            var err = null;
                                                                                                            var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                                                            res.json(responseFormatDict);          // response
                                                                                                            logger.log('info', responseFormatDict);
                                                                                                        }
                                                                                                        else {                      // if there is error.....
                                                                                                            var data = null;
                                                                                                            var status = false;
                                                                                                            var message = "err";
                                                                                                            var err = err;
                                                                                                            var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                                                            res.json(responseFormatDict);    //response
                                                                                                            console.log('Error during record insertion : ' + err);
                                                                                                            logger.log('info', ('Error during record insertion : ' + err));
                                                                                                            logger.log('info', responseFormatDict);
                                                                                                        }
                                                                                                    }
                                                                                                    catch (e) {
                                                                                                        catchFormat(e, res);
                                                                                                    }
                                                                                                });
                                                                                            }
                                                                                        }
                                                                                        catch (e) {
                                                                                            catchFormat(e, res);
                                                                                        }
                                                                                    });
                                                                                }
                                                                            }
                                                                            catch (e) {
                                                                                catchFormat(e, res);
                                                                            }
                                                                        });
                                                                    }
                                                                    else {
                                                                        //if the id is existing then update the existing ones
                                                                        try {
                                                                            console.log("dxfgj")
                                                                            Expense.findOne({ _id: req.body._id }, (err, docs) => { // retreiving the Expense by _id
                                                                                try {
                                                                                    if (((docs.expenseStatusID) == 4) || ((docs.expenseStatusID) == 6) || ((docs.expenseStatusID) == 7)) {
                                                                                        var data = {        // data that need to be updated
                                                                                            updatedDate: currdatetime,
                                                                                            managerReferenceID: req.body.managerReferenceID,
                                                                                            expensePurpose: req.body.expensePurpose,
                                                                                        }
                                                                                        Expense.findOneAndUpdate({ _id: req.body._id }, data, { new: true }, (err, doc) => {      // find the id and update the data
                                                                                            try {
                                                                                                if (!err) { //if there is no error...
                                                                                                    var data = { expenseObjectId: doc._id };
                                                                                                    var status = true;
                                                                                                    var message = "updated successfully to expense table";
                                                                                                    var err = null;
                                                                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                                    res.json(responseFormatDict);
                                                                                                    logger.log('info', responseFormatDict);
                                                                                                }
                                                                                                else {  //if there is error...
                                                                                                    var data = null;
                                                                                                    var status = false;
                                                                                                    var message = "err";
                                                                                                    var err = err;
                                                                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                                                    res.json(responseFormatDict);
                                                                                                    logger.log('info', responseFormatDict);
                                                                                                    logger.log('info', err);
                                                                                                }
                                                                                            }
                                                                                            catch (e) {
                                                                                                catchFormat(e, res);
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                    else {
                                                                                        var data = null;
                                                                                        var status = false;
                                                                                        var message = "Record not found";
                                                                                        var err = "null";
                                                                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                        res.json(responseFormatDict);
                                                                                        logger.log('info', responseFormatDict);
                                                                                    }
                                                                                }
                                                                                catch (e) {
                                                                                    catchFormat(e, res);
                                                                                }
                                                                            })
                                                                        }
                                                                        catch (e) {
                                                                            catchFormat(e, res);
                                                                        }
                                                                    }
                                                                }
                                                                else {
                                                                    var data = null;
                                                                    var status = false;
                                                                    var message = "expenseID is required";
                                                                    var err = err;
                                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                    res.json(responseFormatDict);
                                                                    logger.log('info', responseFormatDict);
                                                                    logger.log('info', err);
                                                                }
                                                            }
                                                            else {
                                                                var data = null;
                                                                var status = false;
                                                                var message = "employeeName is required";
                                                                var err = err;
                                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                res.json(responseFormatDict);
                                                                logger.log('info', responseFormatDict);
                                                                logger.log('info', err);
                                                            }
                                                        }
                                                        else {
                                                            var data = null;
                                                            var status = false;
                                                            var message = "employeeName is required";
                                                            var err = err;
                                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                            res.json(responseFormatDict);
                                                            logger.log('info', responseFormatDict);
                                                            logger.log('info', err);
                                                        }
                                                    }
                                                    else {
                                                        var data = null;
                                                        var status = false;
                                                        var message = "expensePurpose is required";
                                                        var err = err;
                                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                        res.json(responseFormatDict);
                                                        logger.log('info', responseFormatDict);
                                                        logger.log('info', err);
                                                    }
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "expensePurpose is required";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    logger.log('info', err);
                                                }
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "managerReferenceID is required";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "managerReferenceId is required";
                                            var err = err;
                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                            logger.log('info', err);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        // logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    //logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
}
function saveExpenseLine(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        if ((req.body.expenseID != null)) {
                                            if (((req.body.expenseID).length != 0)) {
                                                if ((req.body.receiptID != null)) {
                                                    if ((req.body.url != null)) {
                                                        if ((req.body.comments != null)) {
                                                            if ((req.body.otherExpense != null)) {
                                                                var currdatetime = Date.now();
                                                                console.log("..." + currdatetime);
                                                                logger.log('info', currdatetime);
                                                                if ((req.body._id == null) || (req.body._id == "")) { //if the id is not existing then add it as a new Expense...
                                                                    var currencyid = req.body.currency;
                                                                    // var expenseLine = new ExpenseLine();
                                                                    //var currdatetime =  todate.getTime();;
                                                                    // var expenseStatus=req.body.expenseLineStatusID;
                                                                    //console.log("data is"+expenseStatus);
                                                                    ExpenseLineStatus.findOne({ expenseLineStatus: "Submitted" }, { _id: 0, ExpenseLineStatus: 0 }, (err, docs1) => {
                                                                        try {
                                                                            if (!err) {
                                                                                // var expenseType=req.body.expenseType;
                                                                                //console.log("data is"+docs);
                                                                                // console.log("hh"+req.body.date.day)
                                                                                //  let date = new Date(req.body.date);
                                                                                //document.Date = date.toISOString();
                                                                                //date = new Date(req.body.date);
                                                                                ExpenseType.findOne({ ID: req.body.expenseType }, (err, docs2) => {
                                                                                    try {
                                                                                        console.log("expenseTYpe=>" + typeof (docs2));
                                                                                        logger.log('info', ("expenseTYpe=>" + typeof (docs2)));
                                                                                        if (docs2 == null) {
                                                                                            console.log("expenseTYpe=>" + docs2);
                                                                                            var data = null;
                                                                                            var status = false;
                                                                                            var message = "expensetype is not found";
                                                                                            var err = err;
                                                                                            var responseFormatDict = responseFormat(data, message, status, err);  // calling the function responseFormat()
                                                                                            res.json(responseFormatDict);
                                                                                            logger.log('info', responseFormatDict);
                                                                                            logger.log('info', err);
                                                                                        }
                                                                                        else {
                                                                                            Currency.findOne({ ID: req.body.currency }, (err, docs) => {
                                                                                                try {
                                                                                                    if (docs == null) {
                                                                                                        var data = null;
                                                                                                        var status = false;
                                                                                                        var message = "currency is not found";
                                                                                                        var err = err;
                                                                                                        var responseFormatDict = responseFormat(data, message, status, err);  // calling the function responseFormat()
                                                                                                        res.json(responseFormatDict);
                                                                                                        logger.log('info', responseFormatDict);
                                                                                                        logger.log('info', err);
                                                                                                    }
                                                                                                    else {
                                                                                                        var date = new Date(req.body.date);
                                                                                                        //var day=date.getMonth();
                                                                                                        var month = date.getUTCMonth() + 1; //months from 1-12
                                                                                                        var day = date.getUTCDate() + 1;
                                                                                                        var year = date.getUTCFullYear();
                                                                                                        //var dateObj=year+"/"+month+"/"+(day+1);
                                                                                                        console.log("day" + day);
                                                                                                        console.log("month" + month);
                                                                                                        console.log("year" + year);
                                                                                                        if (day == 31) {
                                                                                                            if (month == 11) {
                                                                                                                //console.log("date"+month);
                                                                                                                var dateObj = (year) + "/" + (month + 1) + "/" + (2);
                                                                                                            }
                                                                                                            else if (month != 12) {
                                                                                                                var dateObj = year + "/" + (month + 1) + "/" + (1);
                                                                                                            }
                                                                                                            else {
                                                                                                                var dateObj = (year + 1) + "/" + (1) + "/" + (1);
                                                                                                            }
                                                                                                        }
                                                                                                        else if (day == 30) {
                                                                                                            if ((month == 4) || (month == 6) || (month == 9) || (month == 11)) {
                                                                                                                var dateObj = year + "/" + (month + 1) + "/" + (1);
                                                                                                            }
                                                                                                            else {
                                                                                                                var dateObj = year + "/" + month + "/" + (day + 1);
                                                                                                            }
                                                                                                        }
                                                                                                        else if (day == 29) {
                                                                                                            if (month == 2) {
                                                                                                                var dateObj = year + "/" + (month + 1) + "/" + (1);
                                                                                                            }
                                                                                                            else {
                                                                                                                var dateObj = year + "/" + month + "/" + (day + 1);
                                                                                                            }
                                                                                                        }
                                                                                                        else if (day == 32) {
                                                                                                            if (month != 12) {
                                                                                                                var dateObj = year + "/" + (month + 1) + "/" + (2);
                                                                                                            }
                                                                                                            else if (month == 12) {
                                                                                                                var dateObj = (year + 1) + "/" + 1 + "/" + (2);
                                                                                                            }
                                                                                                            else {
                                                                                                                var dateObj = (year) + "/" + 1 + "/" + (2);
                                                                                                            }
                                                                                                        }
                                                                                                        else {
                                                                                                            var dateObj = year + "/" + month + "/" + (day + 1);
                                                                                                        }
                                                                                                        dateObj.toString();
                                                                                                        console.log("the date is   " + dateObj);
                                                                                                        console.log("the date is....   " + req.body.date);
                                                                                                        logger.log('info', ("the date is   " + dateObj));
                                                                                                        logger.log('info', ("the date is....   " + req.body.date));
                                                                                                        var id = mongoose.Types.ObjectId(req.body.expenseID);
                                                                                                        //console.log("hhhrr"+dateObj);
                                                                                                        var data2 = docs1.ID;
                                                                                                        //console.log("tgggb"+ISODate(currdatetime));
                                                                                                        var expenseLine = new ExpenseLine();
                                                                                                        expenseLine.expenseLineStatusID = data2;
                                                                                                        expenseLine.currency = req.body.currency;
                                                                                                        expenseLine.expenseID = id;
                                                                                                        expenseLine.expenseType = req.body.expenseType;
                                                                                                        expenseLine.merchant = req.body.merchant;
                                                                                                        expenseLine.costCenter = req.body.costCenter;
                                                                                                        expenseLine.noOfEmployee = req.body.noOfEmployee;
                                                                                                        expenseLine.nameOfEmployee = req.body.nameOfEmployee;
                                                                                                        expenseLine.location = req.body.location;
                                                                                                        expenseLine.amount = req.body.amount;
                                                                                                        expenseLine.comments = req.body.comments;
                                                                                                        expenseLine.otherExpense = req.body.otherExpense;
                                                                                                        //expenseLine.expenseLineStatusID= ;
                                                                                                        expenseLine.receiptID = req.body.receiptID;
                                                                                                        expenseLine.date = req.body.date;
                                                                                                        expenseLine.url = req.body.url;
                                                                                                        expenseLine.updatedDate = currdatetime;
                                                                                                        expenseLine.createdDate = currdatetime;
                                                                                                        expenseLine.updatedBy = req.body.updatedBy;
                                                                                                        expenseLine.createdBy = req.body.createdBy;
                                                                                                        expenseLine.save((err, doc) => {    //saving datas
                                                                                                            try {
                                                                                                                if (!err) {          //if there is no error...
                                                                                                                    console.log('Inserted succeesfylly: ');
                                                                                                                    var data = doc;
                                                                                                                    var status = true;
                                                                                                                    var message = "inserted successfully to expenseline table";
                                                                                                                    var err = null;
                                                                                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                                                    res.json(responseFormatDict);
                                                                                                                    logger.log('info', responseFormatDict);
                                                                                                                }
                                                                                                                else {      //error then.....
                                                                                                                    console.log('Error during record insertion : ' + err);
                                                                                                                    var data = null;
                                                                                                                    var status = false;
                                                                                                                    var message = "err";
                                                                                                                    var err = err;
                                                                                                                    var responseFormatDict = responseFormat(data, message, status, err);  // calling the function responseFormat()
                                                                                                                    res.json(responseFormatDict);
                                                                                                                    logger.log('info', responseFormatDict);
                                                                                                                    logger.log('info', err);
                                                                                                                }
                                                                                                            }
                                                                                                            catch (e) {
                                                                                                                catchFormat(e, res);
                                                                                                            }
                                                                                                        });
                                                                                                    }
                                                                                                }
                                                                                                catch (e) {
                                                                                                    catchFormat(e, res);
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    }
                                                                                    catch (e) {
                                                                                        catchFormat(e, res);
                                                                                    }
                                                                                });
                                                                            }
                                                                            else {
                                                                                var data = null;
                                                                                var status = false;
                                                                                var message = "err";
                                                                                var err = err;
                                                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                                res.json(responseFormatDict);
                                                                                logger.log('info', responseFormatDict);
                                                                                logger.log('info', err);
                                                                            }
                                                                        }
                                                                        catch (e) {
                                                                            catchFormat(e, res);
                                                                        }
                                                                    });
                                                                }
                                                                else {
                                                                    try {
                                                                        var data1;
                                                                        var status;
                                                                        var expense = mongoose.Types.ObjectId(req.body.expenseID);
                                                                        var expenseline = mongoose.Types.ObjectId(req.body._id);
                                                                        ExpenseLine.findOne({ _id: req.body._id }, (err, docs12) => {
                                                                            try {
                                                                                //var data1;
                                                                                Expense.findOne({ _id: req.body.expenseID }, (err, docs) => {
                                                                                    try {
                                                                                        console.log("req body" + JSON.stringify(req.body));
                                                                                        //if the id is existing then update the existing ones
                                                                                        console.log("expenseLineStatusID" + docs12.expenseLineStatusID);
                                                                                        if ((docs12.expenseLineStatusID) == 3) {
                                                                                            status = 4;
                                                                                            ExpenseLine.countDocuments({ $and: [{ expenseID: expense }, { expenseLineStatusID: 3 }] }, (err, count2) => {
                                                                                                try {
                                                                                                    if (count2 == 1) {
                                                                                                        if ((docs.expenseStatusID) == 4) {
                                                                                                            var data = {        //data wrapper send to findOneAndUpdate to update
                                                                                                                updatedDate: currdatetime,
                                                                                                                expenseStatusID: 7,
                                                                                                                reSubmittedToManager: true
                                                                                                            }
                                                                                                        }
                                                                                                        else if ((docs.expenseStatusID) == 6) {
                                                                                                            var data = {        //data wrapper send to findOneAndUpdate to update
                                                                                                                updatedDate: currdatetime,
                                                                                                                expenseStatusID: 7,
                                                                                                                reSubmittedToAdmin: true
                                                                                                            }
                                                                                                        }
                                                                                                        Expense.findOneAndUpdate({ _id: expense }, data, { new: true }, (err, doc) => { })
                                                                                                    }
                                                                                                }
                                                                                                catch (e) {
                                                                                                    catchFormat(e, res);
                                                                                                }
                                                                                            })
                                                                                        }
                                                                                        else {
                                                                                            status = 1;
                                                                                        }
                                                                                        console.log("status" + status);
                                                                                        data1 = {        //data wrapper send to findOneAndUpdate to update
                                                                                            expenseType: req.body.expenseType,
                                                                                            merchant: req.body.merchant,
                                                                                            updatedDate: currdatetime,
                                                                                            noOfEmployee: req.body.noOfEmployee,
                                                                                            nameOfEmployee: req.body.nameOfEmployee,
                                                                                            location: req.body.location,
                                                                                            currency: req.body.currency,
                                                                                            amount: req.body.amount,
                                                                                            costCenter: req.body.costCenter,
                                                                                            comments: req.body.comments,
                                                                                            receiptID: req.body.receiptID,
                                                                                            url: req.body.url,
                                                                                            date: req.body.date,
                                                                                            expenseLineStatusID: status,
                                                                                            otherExpense: req.body.otherExpense
                                                                                        }
                                                                                        ExpenseLine.findOneAndUpdate({ _id: req.body._id }, data1, (err, doc) => {
                                                                                            console.log("after updatimg" + doc);
                                                                                            ////response
                                                                                        }).then(function () {
                                                                                            try {   // find the id and update the data
                                                                                                //if there is no error...
                                                                                                //hghgjyuuiyuyhghghghhhhhgghh
                                                                                                console.log(" id inside update", req.body._id);//console
                                                                                                console.log(" data", data1);
                                                                                                var data = null;
                                                                                                var status = true;
                                                                                                var message = "updated successfully to expenseline table";
                                                                                                var err = null;
                                                                                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                                res.json(responseFormatDict);
                                                                                                logger.log('info', responseFormatDict);
                                                                                                //logger.log('info',err);
                                                                                            }
                                                                                            catch (e) {
                                                                                                catchFormat(e, res);
                                                                                            }
                                                                                        })
                                                                                    }
                                                                                    catch (e) {
                                                                                        catchFormat(e, res);
                                                                                    }
                                                                                })
                                                                            }
                                                                            catch (e) {
                                                                                catchFormat(e, res);
                                                                            }
                                                                        })
                                                                    }
                                                                    catch (e) {
                                                                        catchFormat(e, res);
                                                                    }
                                                                }
                                                            }
                                                            else {
                                                                var data = null;
                                                                var status = false;
                                                                var message = "otherExpense is required";
                                                                var err = err;
                                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                res.json(responseFormatDict);
                                                                logger.log('info', responseFormatDict);
                                                                logger.log('info', err);
                                                            }
                                                        }
                                                        else {
                                                            var data = null;
                                                            var status = false;
                                                            var message = "comment is required";
                                                            var err = err;
                                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                            res.json(responseFormatDict);
                                                            logger.log('info', responseFormatDict);
                                                            logger.log('info', err);
                                                        }
                                                    }
                                                    else {
                                                        var data = null;
                                                        var status = false;
                                                        var message = "receiptURL is required";
                                                        var err = err;
                                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                        res.json(responseFormatDict);
                                                        logger.log('info', responseFormatDict);
                                                        logger.log('info', err);
                                                    }
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "receiptID is required";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    logger.log('info', err);
                                                }
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "expenseID is required";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "expenseID is required";
                                            var err = err;
                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                            logger.log('info', err);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        // logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    // logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        var data = null;
        var status = false;
        var message = "something went ...wrong";
        var err = e;
        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
        res.json(responseFormatDict);
        logger.log('info', responseFormatDict);
        logger.log('info', err);
    }
}
function job1(docs12, docs, expense, currdatetime, req) {
    try {
        // console.log("the fffrffff"+JSON.stringify(req.body));
        var status;
        console.log("Expense Status" + docs)              //if the id is existing then update the existing ones
        logger.log('info', docs);
        if ((docs12.expenseLineStatusID) == 3) {
            status = 4;
            ExpenseLine.countDocuments({ $and: [{ expenseID: expense }, { expenseLineStatusID: 3 }] }, (err, count2) => {
                try {
                    if (!err) {
                        if (count2 == 1) {
                            if ((docs.expenseStatusID) == 4) {
                                var data = {        //data wrapper send to findOneAndUpdate to update
                                    updatedDate: currdatetime,
                                    expenseStatusID: 7,
                                    reSubmittedToManager: true
                                }
                            }
                            else if ((docs.expenseStatusID) == 6) {
                                var data = {        //data wrapper send to findOneAndUpdate to update
                                    updatedDate: currdatetime,
                                    expenseStatusID: 7,
                                    reSubmittedToAdmin: true
                                }
                            }
                            Expense.findOneAndUpdate({ _id: expense }, data, { new: true }, (err, doc) => {
                                try { } catch (e) { catchFormat(e, res); }
                            })
                        }
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "err";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                catch (e) {
                    catchFormat(e, res);
                }
            })
        }
        else {
            status = 1;
        }
        console.log("status" + status)
        logger.log('info', ("status" + status));
        var data1 = {        //data wrapper send to findOneAndUpdate to update
            expenseType: req.body.expenseType,
            merchant: req.body.merchant,
            updatedDate: currdatetime,
            noOfEmployee: req.body.noOfEmployee,
            nameOfEmployee: req.body.nameOfEmployee,
            location: req.body.location,
            currency: req.body.currency,
            amount: req.body.amount,
            costCenter: req.body.costCenter,
            date: req.body.date,
            comments: req.body.comments,
            receiptID: req.body.receiptID,
            receiptURL: req.body.receiptURL,
            expenseLineStatusID: status,
            webURL: req.body.webURL
        }
        console.log("nivia james" + data1.location);
        logger.log('info', data1.location);
        /*setTimeout(function() {
            resolve(data1);
        }, 1000);*/
        return data1;
    }
    catch (e) {
        catchFormat(e, res);
    }
}
function searchByDaterange(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        if ((typeof (req.body.fromDate) != 'undefined') || (req.body.fromDate != null)) {
                                            if ((req.body.fromDate).length != 0) {
                                                if ((typeof (req.body.toDate) != 'undefined') || (req.body.toDate != null)) {
                                                    if ((req.body.toDate).length != 0) {
                                                        console.log("ghhhjjj" + req.body.employeeID)
                                                        date = new Date(req.body.fromDate).setHours(0, 0, 0, 0)
                                                        endDate = new Date(req.body.toDate).setHours(23, 59, 59, 59)
                                                        var mysort = { updatedDate: -1 };
                                                        var item = [];
                                                        Expense.find({
                                                            $and: [{
                                                                updatedDate: {
                                                                    $gte: date,
                                                                    $lte: endDate
                                                                }
                                                            }, { employeeID: req.body.employeeID }]
                                                        }, { '__v': 0 }, { sort: mysort }, (err, docs) => {
                                                            try {
                                                                docs.forEach(element => {
                                                                    try {
                                                                        if (element.deletionStatus != true) {
                                                                            item.push(element);
                                                                        }
                                                                    }
                                                                    catch (e) {
                                                                        catchFormat(e, res);
                                                                    }
                                                                })
                                                                if (!err) {
                                                                    var data1 = item;
                                                                    //console.log("rr"+data1);
                                                                    Expense.countDocuments({
                                                                        $and: [{
                                                                            updatedDate: {
                                                                                $gte: date,
                                                                                $lte: endDate
                                                                            }
                                                                        }, { employeeID: req.body.employeeID }, { deletionStatus: { $ne: true } }]
                                                                    }, (err, count) => {
                                                                        try {
                                                                            if (data1 != "") {
                                                                                console.log("count" + count);
                                                                                var data3 = data1[0].expenseStatusID;
                                                                                var data5 = data1[0].managerReferenceID
                                                                                findStatus(data5, data3, 0, data1, res, count);
                                                                            }
                                                                            //console.log("lst"+data1);
                                                                            else {
                                                                                var data = null;
                                                                                var status = false;
                                                                                var message = "Record not found";
                                                                                console.log(message);
                                                                                var err = err;
                                                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                                res.json(responseFormatDict);
                                                                                logger.log('info', responseFormatDict);
                                                                                logger.log('info', err);
                                                                            }
                                                                        }
                                                                        catch (e) {
                                                                            catchFormat(e, res);
                                                                        }
                                                                    })
                                                                }
                                                                else {
                                                                    var data = null;
                                                                    var status = false;
                                                                    var message = "err";
                                                                    console.log(message);
                                                                    var err = err;
                                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                    res.json(responseFormatDict);
                                                                    logger.log('info', responseFormatDict);
                                                                    logger.log('info', err);
                                                                }
                                                            }
                                                            catch (e) {
                                                                catchFormat(e, res);
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        var data = null;
                                                        var status = false;
                                                        var message = "toDate is required";
                                                        console.log(message);
                                                        var err = err;
                                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                        res.json(responseFormatDict);
                                                        logger.log('info', responseFormatDict);
                                                        logger.log('info', err);
                                                    }
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "toDate is required";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    logger.log('info', err);
                                                }
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "fromDate is required";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "fromDate is required";
                                            var err = err;
                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                            logger.log('info', err);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        // logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    //  logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
}
function deleteExpense(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            try {
                                if (!err) {
                                    if (doc != null) {
                                        if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                            if ((req.body._id) != null) {
                                                if ((req.body._id).length != 0) {
                                                    console.log(req.body);
                                                    var post_data = req.body;
                                                    var empID = post_data.empId;
                                                    var objid = post_data._id;
                                                    var newobjid = mongoose.Types.ObjectId(objid);
                                                    var data1 = {
                                                        deletionStatus: true
                                                    }
                                                    Expense.findOne({ _id: req.body._id }, (err, docs) => {
                                                        try {
                                                            if (docs != null) {
                                                                console.log(docs);
                                                                if (docs.expenseStatusID == "7") {
                                                                    console.log("rrrrrrrrrr" + docs.expenseStatusID);
                                                                    logger.log('info', docs.expenseStatusID);
                                                                    Expense.findOneAndUpdate({ _id: objid }, data1, { new: true }, (err, docs) => {
                                                                        try {
                                                                            if (!err) {
                                                                                if (docs != null) {
                                                                                    console.log('obj id=' + docs._id);
                                                                                    logger.log('info', ('obj id=' + docs._id));
                                                                                    var data = docs;
                                                                                    console.log(data);
                                                                                    logger.log('info', data);
                                                                                    //console.log(typeof(newobjid));
                                                                                    ExpenseLine.deleteMany({ expenseID: newobjid }, (error, docs1) => {
                                                                                        try {
                                                                                            if (!error) {
                                                                                                console.log("docum" + docs1);
                                                                                                logger.log('info', ("docum" + docs1));
                                                                                                if (docs1 != null) {
                                                                                                    var data = null;
                                                                                                    var status = true;
                                                                                                    var message = "deleted";
                                                                                                    var err = null;
                                                                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                                    res.json(responseFormatDict);
                                                                                                    logger.log('info', responseFormatDict);
                                                                                                } else {
                                                                                                    var data = null;
                                                                                                    var status = false;
                                                                                                    var message = "Record not found";
                                                                                                    var err = null;
                                                                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                                    res.json(responseFormatDict);
                                                                                                    logger.log('info', responseFormatDict);
                                                                                                }
                                                                                            } else {
                                                                                                var data = null;
                                                                                                var status = false;
                                                                                                var message = "error";
                                                                                                var err = error;
                                                                                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                                res.json(responseFormatDict);
                                                                                                logger.log('info', responseFormatDict);
                                                                                                logger.log('info', err);
                                                                                            }
                                                                                        }
                                                                                        catch (e) {
                                                                                            catchFormat(e, res);
                                                                                        }
                                                                                    });
                                                                                    //logger.log('info',"new data"+docs);
                                                                                }
                                                                                else {
                                                                                    // console.log("null->"+data)
                                                                                    var data = null;
                                                                                    var status = false;
                                                                                    var message = "document not found";
                                                                                    var err = null;
                                                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                    res.json(responseFormatDict);
                                                                                    logger.log('info', responseFormatDict);
                                                                                    // logger.log('info',err);
                                                                                }
                                                                            }
                                                                        }
                                                                        catch (e) {
                                                                            catchFormat(e, res);
                                                                        }
                                                                    })
                                                                } else {
                                                                    var data = null;
                                                                    var status = false;
                                                                    var message = "Not an unsubmitted expense: not be deleted";
                                                                    var err = null;
                                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                    res.json(responseFormatDict);
                                                                    logger.log('info', responseFormatDict);
                                                                    //logger.log('info',err);
                                                                }
                                                            } else {
                                                                var data = null;
                                                                var status = false;
                                                                var message = "document not found";
                                                                var err = null;
                                                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                res.json(responseFormatDict);
                                                                logger.log('info', responseFormatDict);
                                                                //logger.log('info',err);
                                                            }
                                                        }
                                                        catch (e) {
                                                            catchFormat(e, res);
                                                        }
                                                    });
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "id is required";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    logger.log('info', err);
                                                }
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "id is required";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "Access Denied";
                                            var err = "null";
                                            var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                            logger.log('info', err);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Employee ID is not valid";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "err";
                                    var err = err;
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    logger.log('info', err);
                                }
                            }
                            catch (e) {
                                catchFormat(e, res);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
}
function findStatus(data5, doc0, index, data1, res, count) {
    //console.log("status"+doc0);
    try {
        console.log("status" + count);
        logger.log('info', ("status" + count));
        ExpenseStatus.findOne({ ID: doc0 }, (err, docs1) => {
            if (!err) {
                try {
                    console.log(",moeeee" + docs1);
                    logger.log('info', docs1);
                    data1[index].expenseStatusID = docs1.Description;
                    Manager.findOne({ _id: data5 }, (err, docs) => {
                        try {
                            if (docs == null) {
                                var data = null;
                                var status = true;
                                var message = "Manager is not found";
                                var err = "null";
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                            }
                            else {
                                console.log("manager" + docs);
                                logger.log('info', ("manager" + docs));
                                console.log("nnnnn" + docs.firstName + "," + docs.lastName);
                                logger.log('info', ("nnnnn" + docs.firstName + "," + docs.lastName));
                                data1[index].managerReferenceID = docs.lastName + "," + docs.firstName;
                                index = index + 1;
                                //console.log("dataas"+data1[index]);
                                //console.log("index"+index);
                                // console.log("count"+count);
                                // logger.log('info',("dataas"+data1[index]));
                                //logger.log('info',("index"+index));
                                //logger.log('info',("count"+count));
                                if (index < count) {
                                    findStatus(data1[index].managerReferenceID, data1[index].expenseStatusID, index, data1, res, count)
                                }
                                else {
                                    var data = data1;
                                    var status = true;
                                    var message = "Record found";
                                    console.log(message);
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                }
                            }
                        }
                        catch (e) {
                            catchFormat(e, res);
                        }
                    })
                }
                catch (e) {
                    catchFormat(e, res);
                }
            }
        })
    }
    catch (e) {
        catchFormat(e, res);
    }
}
function findExpenseLineStatus(expenseType, currency, doc0, index, data1, res, count) {
    try {
        console.log("status" + doc0);
        logger.log('info', ("status" + doc0));
        ExpenseLineStatus.findOne({ ID: doc0 }, (err, docs1) => {
            if (!err) {
                try {
                    if (docs1 == null) {
                        var data = null;
                        var status = false;
                        var message = "ExpenseLine status  is not found";
                        var err = "null";
                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                    }
                    else {
                        console.log(",....." + docs1);
                        logger.log('info', (",....." + docs1));
                        data1[index].expenseLineStatusID = docs1.expenseLineStatus;
                        ExpenseType.findOne({ ID: expenseType }, (err, docs2) => {
                            try {
                                if (docs2 == null) {
                                    var data = null;
                                    var status = true;
                                    var message = "Expense Type is not found";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                }
                                //console.log("manager"+docs2);
                                else {
                                    //console.log("expebbtyp"+docs2.expenseType)
                                    // if(docs2.ID=="6"){
                                    //    data1[index].expenseType=data1[index].otherExpense;
                                    //// }
                                    // else{
                                    data1[index].expenseType = docs2.expenseType;
                                    //  }
                                    Currency.findOne({ ID: currency }, (err, docs) => {
                                        try {
                                            if (docs == null) {
                                                var data = null;
                                                var status = true;
                                                var message = "Currency is not found is not found";
                                                var err = "null";
                                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                            }
                                            else {
                                                data1[index].currency = docs.currency;
                                                // console.log("manager"+docs);
                                                index = index + 1;
                                                if (index < count) {
                                                    findExpenseLineStatus(data1[index].expenseType, data1[index].currency, data1[index].expenseLineStatusID, index, data1, res, count)
                                                }
                                                else {
                                                    var data = data1;
                                                    var status = true;
                                                    var message = "Record found";
                                                    var err = "null";
                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                }
                                            }
                                        }
                                        catch (e) {
                                            catchFormat(e, res);
                                        }
                                    })
                                }
                            }
                            catch (e) {
                                catchFormat(e, res);
                            }
                        })
                    }
                }
                catch (e) {
                    catchFormat(e, res);
                }
            }
            else {
                var data = null;
                var status = true;
                var message = "error";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        })
    }
    catch (e) {
        catchFormat(e, res);
    }
}
function getManagers(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            try {
                                if (!err) {
                                    if (doc != null) {
                                        if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                            if ((req.body.employeeEmail) != null) {
                                                if ((req.body.employeeEmail).length != 0) {
                                                    var userName = req.body.employeeEmail.toLowerCase();
                                                    Manager.find({ emailID: { $ne: userName } }, (err, docs) => {
                                                        try {
                                                            if (!err) {
                                                                var data = docs;
                                                                var status = true;
                                                                var message = "Record found";
                                                                var err = "null";
                                                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                res.json(responseFormatDict);
                                                                logger.log('info', responseFormatDict);
                                                                //logger.log('info',err);
                                                            }
                                                            else {
                                                                var data = null;
                                                                var status = false;
                                                                var message = "err";
                                                                var err = err;
                                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                res.json(responseFormatDict);
                                                                logger.log('info', responseFormatDict);
                                                                logger.log('info', err);
                                                            }
                                                        }
                                                        catch (e) {
                                                            catchFormat(e, res);
                                                        }
                                                    });
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "employeeEmail is required";
                                                    var err = "null";
                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                }
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "employeeEmail is required";
                                                var err = "null";
                                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "Access Denied";
                                            var err = "null";
                                            var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                            //  logger.log('info',err);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Employee ID is not valid";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //   logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "err";
                                    var err = err;
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    logger.log('info', err);
                                }
                            }
                            catch (e) {
                                catchFormat(e, res);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
}

function DeleteExpenseLine(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        if ((typeof (req.body._id) != 'undefined') || (req.body._id != null)) {
                                            if ((req.body._id).length != 0) {
                                                ExpenseLine.findByIdAndRemove({ _id: req.body._id }, (err, docs) => {
                                                    try {
                                                        if (docs != null) {
                                                            if (!err) {
                                                                var data = docs;
                                                                var status = true;
                                                                var message = "Record Deleted";
                                                                var err = "null";
                                                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                res.json(responseFormatDict);
                                                                logger.log('info', responseFormatDict);
                                                                // logger.log('info',err);
                                                            }
                                                            else {
                                                                var data = null;
                                                                var status = false;
                                                                var message = "err";
                                                                var err = err;
                                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                res.json(responseFormatDict);
                                                                logger.log('info', responseFormatDict);
                                                                logger.log('info', err);
                                                            }
                                                        }
                                                        else {
                                                            var data = null;
                                                            var status = false;
                                                            var message = "Record not found";
                                                            var err = "null";
                                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                            res.json(responseFormatDict);
                                                            logger.log('info', responseFormatDict);
                                                            //logger.log('info',err);
                                                        }
                                                    }
                                                    catch (e) {
                                                        catchFormat(e, res);
                                                    }
                                                });
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "ID is required";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "ID is required";
                                            var err = err;
                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                            logger.log('info', err);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //  logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    //  logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
}

function searchByExpense(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        //console.log("req"+(req.body).countDocuments);
                                        if ((typeof (req.body.expenseID) != 'undefined') || (req.body.expenseID != null)) {
                                            //console.log("ggh"+req.body.expenseID);
                                            if ((req.body.expenseID).length != 0) {
                                                //  console.log("cc"+req.body.employeeID);
                                                var item = [];
                                                var mysort = { updatedDate: -1 };
                                                Expense.find({ $and: [{ expenseID: req.body.expenseID }, { employeeID: req.body.employeeID }] }, (err, docs) => {
                                                    if (!err) {
                                                        try {
                                                            docs.forEach(element => {
                                                                try {
                                                                    if (element.deletionStatus != true) {
                                                                        item.push(element);
                                                                    }
                                                                }
                                                                catch (e) {
                                                                    catchFormat(e, res);
                                                                }
                                                            })
                                                            console.log("docs=>" + docs);
                                                            logger.log('info', ("docs=>" + docs));
                                                            if (docs != "") {
                                                                var data1 = item;
                                                                console.log("docs=>" + data1);
                                                                logger.log('info', ("docs=>" + data1));
                                                                let date_format = {};
                                                                date_format.year = 'numeric';
                                                                date_format.month = 'numeric';
                                                                date_format.day = '2-digit';
                                                                date_format.hour = 'numeric';
                                                                date_format.minute = 'numeric';
                                                                date_format.second = 'numeric';
                                                                // var date=data1[0].updatedDate.toString("yyyyMMddHHmmss").
                                                                // replace(/T/, ' ').
                                                                // replace(/\..+/, '');
                                                                console.log("ggg");
                                                                var date = data1[0].updatedDate.toLocaleDateString('us-EN', date_format).toString("yyyyMMddHHmmss");
                                                                console.log(date.replace(",", " "));
                                                                Expense.countDocuments({ $and: [{ expenseID: req.body.expenseID }, { employeeID: req.body.employeeID }, { deletionStatus: { $ne: true } }] }, (err, count) => {
                                                                    try {
                                                                        if (data1 != "" || data1 != null) {
                                                                            //console.log("count"+count);
                                                                            var data3 = data1[0].expenseStatusID;
                                                                            var data5 = data1[0].managerReferenceID
                                                                            findStatus(data5, data3, 0, data1, res, count);
                                                                        }
                                                                        //console.log("lst"+data1);
                                                                        else {
                                                                            var data = null;
                                                                            var status = false;
                                                                            var message = "Record not found";
                                                                            console.log(message);
                                                                            var err = err;
                                                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                            res.json(responseFormatDict);
                                                                            logger.log('info', responseFormatDict);
                                                                            logger.log('info', err);
                                                                        }
                                                                    }
                                                                    catch (e) {
                                                                        catchFormat(e, res);
                                                                    }
                                                                })
                                                            }
                                                            else {
                                                                var data = null;
                                                                var status = false;
                                                                var message = "Record not found";
                                                                console.log(message);
                                                                var err = err;
                                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                res.json(responseFormatDict);
                                                                logger.log('info', responseFormatDict);
                                                                logger.log('info', err);
                                                            }
                                                        }
                                                        catch (e) {
                                                            catchFormat(e, res);
                                                        }
                                                    }
                                                    else {
                                                        var data = null;
                                                        var status = false;
                                                        var message = "err";
                                                        console.log(message);
                                                        var err = err;
                                                        var responseFormatDict = responseFormat(data, message, status, err);
                                                        //calling the function responseFormat()
                                                        res.json(responseFormatDict);
                                                        logger.log('info', responseFormatDict);
                                                        logger.log('info', err);
                                                    }
                                                });
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "ExpenseID is required";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "ExpenseID is required";
                                            var err = err;
                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                            logger.log('info', err);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    //  logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        var data = null;
        var status = false;
        var message = "Something went wrong ";
        var err = e;
        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
        res.json(responseFormatDict);
        logger.log('info', responseFormatDict);
        logger.log('info', err);
    }
}

function verifyAndPay(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        var mysort = { updatedDate: -1 };
                                        Expense.find({ $or: [{ expenseStatusID: 3 }, { expenseStatusID: 8 }] }, { '__v': 0 }, { sort: mysort }, (err, docs) => {
                                            try {
                                                if (!err) {
                                                    var data1 = docs;
                                                    // console.log("rr"+data1);
                                                    Expense.countDocuments({ expenseStatusID: 3 }, (err, count) => {
                                                        try {
                                                            if (data1 != "") {
                                                                console.log("count" + count);
                                                                logger.log('info', ("count" + count));
                                                                var data3 = data1[0].expenseStatusID;
                                                                var data5 = data1[0].managerReferenceID
                                                                findStatus(data5, data3, 0, data1, res, count);
                                                            }
                                                            //console.log("lst"+data1);
                                                            else {
                                                                var data = null;
                                                                var status = false;
                                                                var message = "Record not found";
                                                                var err = err;
                                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                res.json(responseFormatDict);
                                                                logger.log('info', responseFormatDict);
                                                            }
                                                        }
                                                        catch (e) {
                                                            catchFormat(e, res);
                                                        }
                                                    })
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "err";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    logger.log('info', err);
                                                }
                                            }
                                            catch (e) {
                                                catchFormat(e, res);
                                            }
                                        });
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //   logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    // logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        var data = null;
        var status = false;
        var message = "Something went wrong";
        var err = e;
        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
        res.json(responseFormatDict);
        logger.log('info', responseFormatDict);
        logger.log('info', err);
    }
}

function DeclineComment(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        if ((req.body.declineComments) != null) {
                            if ((req.body.declineComments).length != 0) {
                                if ((req.body._id) != null) {
                                    if ((req.body._id).length != 0) {
                                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                                            if (!err) {
                                                if (doc != null) {
                                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                                        console.log("req=>" + JSON.stringify(req.body));
                                                        logger.log('info', ("req=>" + JSON.stringify(req.body)));
                                                        ExpenseLine.find({ _id: req.body._id }, (err, docs) => {
                                                            try {
                                                                if (!err) {
                                                                    if (docs != null) {
                                                                        docs.declineComments = req.body.declineComments;
                                                                        var data = {
                                                                            declineComments: req.body.declineComments
                                                                        }
                                                                        ExpenseLine.findOneAndUpdate({ _id: req.body._id }, data, (err, doc) => {
                                                                            try {
                                                                                console.log("rerrrggygus" + doc);
                                                                                logger.log('info', doc);
                                                                                if (!err) {
                                                                                    var data = null;
                                                                                    var status = true;
                                                                                    var message = "Decline comment send successfully ";
                                                                                    console.log("message=>" + message);
                                                                                    var err = err;
                                                                                    var responseFormatDict = responseFormat(data, message, status, err); //calling the function responseFormat()
                                                                                    res.json(responseFormatDict);
                                                                                    logger.log('info', responseFormatDict);
                                                                                    logger.log('info', err);
                                                                                }
                                                                                else {
                                                                                    var data = null;
                                                                                    var status = false;
                                                                                    var message = "err";
                                                                                    var err = err;
                                                                                    var responseFormatDict = responseFormat(data, message, status, err); //calling the function responseFormat()
                                                                                    res.json(responseFormatDict);
                                                                                    logger.log('info', responseFormatDict);
                                                                                    logger.log('info', err);
                                                                                }
                                                                            }
                                                                            catch (e) {
                                                                                catchFormat(e, res);
                                                                            }
                                                                        })
                                                                    }
                                                                    else {
                                                                        var data = null;
                                                                        var status = false;
                                                                        var message = "Record not found";
                                                                        console.log("message=>" + message);
                                                                        var err = err;
                                                                        var responseFormatDict = responseFormat(data, message, status, err); //calling the function responseFormat()
                                                                        res.json(responseFormatDict);
                                                                        logger.log('info', responseFormatDict);
                                                                        logger.log('info', err);
                                                                    }
                                                                } else {
                                                                    var data = null;
                                                                    var status = false;
                                                                    var message = "err";
                                                                    var err = err;
                                                                    var responseFormatDict = responseFormat(data, message, status, err); //calling the function responseFormat()
                                                                    res.json(responseFormatDict);
                                                                    logger.log('info', responseFormatDict);
                                                                    logger.log('info', err);
                                                                }
                                                            }
                                                            catch (e) {
                                                                catchFormat(e, res);
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        var data = null;
                                                        var status = false;
                                                        var message = "Access Denied";
                                                        var err = "null";
                                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                        res.json(responseFormatDict);
                                                        logger.log('info', responseFormatDict);
                                                        //logger.log('info',err);
                                                    }
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "Employee ID is not valid";
                                                    var err = "null";
                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    // logger.log('info',err);
                                                }
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "err";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                            }
                                        })
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "_id is required";
                                        var err = err;
                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        logger.log('info', err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "_id is required";
                                    var err = err;
                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    logger.log('info', err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "declineComments is required";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        }
                        else {
                            var data = null;
                            var status = false;
                            var message = "declineComments is required";
                            var err = err;
                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                            res.json(responseFormatDict);
                            logger.log('info', responseFormatDict);
                            logger.log('info', err);
                        }
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    } catch (e) {
        var data = null;
        var status = false;
        var message = "Something went wrong";
        var err = e;
        var responseFormatDict = responseFormat(data, message, status, err); //calling the function responseFormat()
        res.json(responseFormatDict);
        logger.log('info', responseFormatDict);
        logger.log('info', err);
    }
}

function getExpenseLine(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        if ((typeof (req.body.expenseID) != 'undefined') || (req.body.expenseID != null)) {
                                            if ((req.body.expenseID).length != 0) {
                                                var id = mongoose.Types.ObjectId(req.body.expenseID);
                                                ExpenseLine.find({ expenseID: id }, (err, docs) => {
                                                    try {
                                                        if (!err) {
                                                            var data1 = docs;
                                                            // console.log("rr"+docs);
                                                            ExpenseLine.countDocuments({ expenseID: id }, (err, count) => {
                                                                try {
                                                                    if (data1 != "") {
                                                                        console.log("count" + count);
                                                                        var data3 = data1[0].expenseLineStatusID;
                                                                        var data5 = data1[0].currency;
                                                                        var data6 = data1[0].expenseType;
                                                                        findExpenseLineStatus(data6, data5, data3, 0, data1, res, count);
                                                                    }
                                                                    //console.log("lst"+data1);
                                                                    else {
                                                                        var data = null;
                                                                        var status = false;
                                                                        var message = "Record not found";
                                                                        var err = err;
                                                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                        res.json(responseFormatDict);
                                                                        logger.log('info', responseFormatDict);
                                                                        logger.log('info', err);
                                                                    }
                                                                }
                                                                catch (e) {
                                                                    catchFormat(e, res);
                                                                }
                                                            })
                                                        }
                                                        else {
                                                            var data = null;
                                                            var status = false;
                                                            var message = "err";
                                                            var err = err;
                                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                            res.json(responseFormatDict);
                                                            logger.log('info', responseFormatDict);
                                                            logger.log('info', err);
                                                        }
                                                    }
                                                    catch (e) {
                                                        catchFormat(e, res);
                                                    }
                                                });
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "expenseID is required";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false
                                            var message = "expenseID is required";
                                            var err = err;
                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                            logger.log('info', err);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //    logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    //  logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        var data = null;
        var status = false
        var message = "Something went wrong";
        var err = e;
        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
        res.json(responseFormatDict);
        logger.log('info', responseFormatDict);
        logger.log('info', err);
    }
}

function ViewOpenRequest(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        console.log("reqt=>" + JSON.stringify(req.body));
                                        logger.log('info', ("reqt=>" + JSON.stringify(req.body)));
                                        var mysort = { updatedDate: -1 };
                                        var item = [];
                                        Expense.find({ employeeID: req.body.employeeID }, {
                                            '__v': 0,    // select keys to return here
                                        }, { sort: mysort }, function (err, doc) {
                                            try {
                                                doc.forEach(element => {
                                                    if (element.deletionStatus != true) {
                                                        item.push(element);
                                                    }
                                                })
                                                //.log("asdfghj"+item);
                                                // console.log("ffffffff"+doc)
                                                // if(doc.deletionStatus!=true){}
                                                // use it here
                                                if (!err) {
                                                    if (item == null) {
                                                        var data = null;
                                                        var status = false;
                                                        var message = "Record not found";
                                                        var err = err;
                                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                        res.json(responseFormatDict);
                                                        logger.log('info', responseFormatDict);
                                                        logger.log('info', err);
                                                    }
                                                    else {
                                                        // console.log("sort=>"+doc);
                                                        var data1 = item;
                                                        Expense.countDocuments({ $and: [{ employeeID: req.body.employeeID }, { deletionStatus: { $ne: true } }] }, (err, count) => {
                                                            try {
                                                                // console.log("sort=>"+doc);
                                                                if (data1 != "") {
                                                                    console.log("count" + count);
                                                                    var data3 = data1[0].expenseStatusID;
                                                                    var data5 = data1[0].managerReferenceID
                                                                    findStatus(data5, data3, 0, data1, res, count);
                                                                }
                                                                else {
                                                                    var data = null;
                                                                    var status = false;
                                                                    var message = "Record not found";
                                                                    var err = err;
                                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                    res.json(responseFormatDict);
                                                                    logger.log('info', responseFormatDict);
                                                                    logger.log('info', err);
                                                                }
                                                            }
                                                            catch (e) {
                                                                catchFormat(e, res);
                                                            }
                                                        })
                                                    }
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "error";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    logger.log('info', err);
                                                }
                                            }
                                            catch (e) {
                                                catchFormat(e, res);
                                            }
                                        });
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        //  var err="null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    // logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        var data = null;
        var status = false;
        var message = "Something went wrong";
        var err = e;
        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
        res.json(responseFormatDict);
        logger.log('info', responseFormatDict);
        logger.log('info', err);
    }
}

function ApproveExpense(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        if ((typeof (req.managerReferenceID) != 'undefined') || (req.body.managerReferenceID != null)) {
                                            if ((req.body.managerReferenceID).length != 0) {
                                                var mysort = { updatedDate: -1 };
                                                Expense.find({ $or: [{ $and: [{ managerReferenceID: req.body.managerReferenceID }, { expenseStatusID: 2 }] }, { $and: [{ managerReferenceID: req.body.managerReferenceID }, { expenseStatusID: 9 }] }, { $and: [{ managerReferenceID: req.body.managerReferenceID }, { expenseStatusID: 1 }] }] }, { '__v': 0 }, { sort: mysort }, (err, docs) => {
                                                    try {
                                                        if (!err) {
                                                            var data1 = docs;
                                                            // console.log("rr"+data1);
                                                            Expense.countDocuments({ $and: [{ managerReferenceID: req.body.managerReferenceID }, { expenseStatusID: 2 }] }, (err, count) => {
                                                                try {
                                                                    if (data1 != "") {
                                                                        console.log("count" + count);
                                                                        logger.log('info', ("count" + count));
                                                                        var data3 = data1[0].expenseStatusID;
                                                                        var data5 = data1[0].managerReferenceID
                                                                        findStatus(data5, data3, 0, data1, res, count);
                                                                    }
                                                                    //console.log("lst"+data1);
                                                                    else {
                                                                        var data = null;
                                                                        var status = false;
                                                                        var message = "Record not found";
                                                                        var err = err;
                                                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                        res.json(responseFormatDict);
                                                                        logger.log('info', responseFormatDict);
                                                                        logger.log('info', err);
                                                                    }
                                                                }
                                                                catch (e) {
                                                                    catchFormat(e, res);
                                                                }
                                                            })
                                                        }
                                                        else {
                                                            var data = null;
                                                            var status = false;
                                                            var message = "err";
                                                            var err = err;
                                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                            res.json(responseFormatDict);
                                                            logger.log('info', responseFormatDict);
                                                            logger.log('info', err);
                                                        }
                                                    }
                                                    catch (e) {
                                                        catchFormat(e, res);
                                                    }
                                                });
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "managerReferenceID is required";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "managerreferenceID is required";
                                            var err = err;
                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                            logger.log('info', err);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    //  logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        var data = null;
        var status = false;
        var message = "Something went wrong";
        var err = e;
        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
        res.json(responseFormatDict);
        logger.log('info', responseFormatDict);
        logger.log('info', err);
    }
}

function SubmitExpense(req, res) {
    try {
        var currDate = Date.now();
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        if ((req.body.mailID) != null) {
                                            if ((req.body.mailID).length != 0) {
                                                if (req.body._id != null) {
                                                    ExpenseStatus.findOne({ expenseStatus: "Submitted" }, { _id: 0 }, (err, docs) => {      // find the id and update the data
                                                        try {
                                                            if (!err) {
                                                                Expense.findOne({ _id: req.body._id }, { __v: 0 }, (err, docs1) => {
                                                                    try {
                                                                        console.log("gfg" + (docs1.expenseID).length);
                                                                        logger.log('info', ("gfg" + (docs1.expenseID).length));
                                                                        if ((docs1.expenseID).length == 0) {
                                                                            Expense.countDocuments({ $and: [{ expenseID: { $ne: "" } }, { expenseID: { $ne: null } }] }, (err, count) => {
                                                                                try {
                                                                                    if (!err) {
                                                                                        if ((req.body.expenseID == "") || (req.body.expenseID == null)) {
                                                                                            var EXID = "EXP00" + (count + 1);
                                                                                            // console.log(",,,,"+docs);
                                                                                            let data = {
                                                                                                expenseID: EXID,
                                                                                                expenseStatusID: docs.ID,
                                                                                                updatedDate: currDate
                                                                                            }
                                                                                            //console.log("count",count);
                                                                                            //console.log("new count",EXID);
                                                                                            if ((typeof (req.body._id) != 'undefined') || (req.body._id != null)) {
                                                                                                if ((req.body._id).length != 0) {
                                                                                                    Expense.findOneAndUpdate({ $and: [{ _id: req.body._id }, { expenseStatusID: { $ne: 2 } }] }, data, { new: true }, (err, doc) => {      // find the id and update the data
                                                                                                        try {
                                                                                                            if (!err) { //if there is no error...
                                                                                                                console.log("vgfbhnj" + doc.employeeName);
                                                                                                                logger.log('info', doc.employeeName);
                                                                                                                if (doc != null) {
                                                                                                                    Manager.findOne({ _id: doc.managerReferenceID }, (err, docs4) => {
                                                                                                                        var sub1 = "Submitted: Expense Reimbursement -" + EXID;
                                                                                                                        var sub2 = "Submitted: Expense Reimbursement - " + EXID;
                                                                                                                        var msg1 = "Dear " + doc.employeeName + "," + "<br>Your Expense " + EXID + " is submitted.  This is under review with the Manager." + "<br><br><br>" + "Regards,";
                                                                                                                        var msg = "Dear " + docs4.firstName + " " + docs4.lastName + "," + "<br>" + EXID + ' is submitted and is  awaiting your approval.' + "<br><br><br>" + "Regards,";
                                                                                                                        emailNotification(req.body.mailID, msg1, sub1)
                                                                                                                        emailNotification(docs4.emailID, msg, sub2)
                                                                                                                        //console.log(" id inside update",req.body._id);
                                                                                                                        var data = null;
                                                                                                                        var status = true;
                                                                                                                        var message = "updated succesfully";
                                                                                                                        var err = null;
                                                                                                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                                                        res.json(responseFormatDict);
                                                                                                                        logger.log('info', responseFormatDict);
                                                                                                                    });
                                                                                                                }
                                                                                                                else {
                                                                                                                    var data = null;
                                                                                                                    var status = true;
                                                                                                                    var message = "Record is not found";
                                                                                                                    var err = null;
                                                                                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                                                    res.json(responseFormatDict);
                                                                                                                    logger.log('info', responseFormatDict);
                                                                                                                }
                                                                                                            }
                                                                                                            else {  //if there is error...
                                                                                                                // console.log('Error during record updation : ' + err);
                                                                                                                var data = null;
                                                                                                                var status = false;
                                                                                                                var message = "err";
                                                                                                                var err = err;
                                                                                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                                                                res.json(responseFormatDict);
                                                                                                                logger.log('info', responseFormatDict);
                                                                                                                logger.log('info', err);
                                                                                                            }
                                                                                                        }
                                                                                                        catch (e) {
                                                                                                            catchFormat(e, res);
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                                else {
                                                                                                    var data1 = null;
                                                                                                    var status = false;
                                                                                                    var message = "expenseID is required";
                                                                                                    var err = err;
                                                                                                    var responseFormatDict = responseFormat(data1, message, status, err);     //calling the function responseFormat()
                                                                                                    res.json(responseFormatDict);
                                                                                                    logger.log('info', responseFormatDict);
                                                                                                    logger.log('info', err);
                                                                                                }
                                                                                            }
                                                                                            else {
                                                                                                var data1 = null;
                                                                                                var status = false;
                                                                                                var message = "expenseID is required";
                                                                                                var err = err;
                                                                                                var responseFormatDict = responseFormat(data1, message, status, err);     //calling the function responseFormat()
                                                                                                res.json(responseFormatDict);
                                                                                                logger.log('info', responseFormatDict);
                                                                                                logger.log('info', err);
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    else {
                                                                                        var data = null;
                                                                                        var status = false;
                                                                                        var message = "err";
                                                                                        var err = err;
                                                                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                                        res.json(responseFormatDict);
                                                                                        logger.log('info', responseFormatDict);
                                                                                        logger.log('info', err);
                                                                                    }
                                                                                }
                                                                                catch (e) {
                                                                                    catchFormat(e, res);
                                                                                }
                                                                            });
                                                                        }
                                                                        else {
                                                                            let data = {
                                                                                expenseStatusID: 9,
                                                                                updatedDate: currDate
                                                                            }
                                                                            //console.log("count",count);
                                                                            // console.log("new count",EXID);
                                                                            Expense.findOneAndUpdate({ $and: [{ _id: req.body._id }, { expenseStatusID: { $ne: 2 } }] }, data, { new: true }, (err, doc) => {      // find the id and update the data
                                                                                try {
                                                                                    if (!err) { //if there is no error...
                                                                                        if (doc != null) {
                                                                                            Manager.findOne({ _id: doc.managerReferenceID }, (err, docs4) => {
                                                                                                try {
                                                                                                    var msg = "Dear " + docs4.firstName + " " + docs4.lastName + ", <br>Expense " + doc.expenseID + " is resubmitted and is awaiting your approval. Please login to the tool for more details.<br><br><br> Regards,";
                                                                                                    var sub = "Resubmitted: Expense Reimbursement - " + doc.expenseID;
                                                                                                    var msg1 = "Dear " + doc.employeeName + ", <br>Your Expense " + doc.expenseID + " is Resubmitted. This is under review with the Manager.<br><br><br>Regards,"
                                                                                                    var sub1 = "Submitted: Expense Reimbursement -" + doc.expenseID
                                                                                                    emailNotification(req.body.mailID, msg1, sub1);
                                                                                                    emailNotification(docs4.emailID, msg, sub);
                                                                                                    console.log(" id inside update", req.body._id);
                                                                                                    var data = null;
                                                                                                    var status = true;
                                                                                                    var message = "Expense ReSubmitted succcessfully";
                                                                                                    var err = null;
                                                                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                                    res.json(responseFormatDict);
                                                                                                    logger.log('info', responseFormatDict);
                                                                                                    //  logger.log('info',err);
                                                                                                }
                                                                                                catch (e) {
                                                                                                    catchFormat(e, res);
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                        else {
                                                                                            var data = null;
                                                                                            var status = false;
                                                                                            var message = "Record not found";
                                                                                            var err = null;
                                                                                            var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                            res.json(responseFormatDict);
                                                                                            logger.log('info', responseFormatDict);
                                                                                            logger.log('info', err);
                                                                                        }
                                                                                    }
                                                                                    else {  //if there is error...
                                                                                        // console.log('Error during record updation : ' + err);
                                                                                        var data = null;
                                                                                        var status = false;
                                                                                        var message = "err";
                                                                                        var err = err;
                                                                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                                        res.json(responseFormatDict);
                                                                                        logger.log('info', responseFormatDict);
                                                                                        logger.log('info', err);
                                                                                    }
                                                                                }
                                                                                catch (e) {
                                                                                    catchFormat(e, res);
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                    catch (e) {
                                                                        catchFormat(e, res);
                                                                    }
                                                                })
                                                            }
                                                            else {
                                                                var data = null;
                                                                var status = false;
                                                                var message = "Submitted status is not found";
                                                                var err = err;
                                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                res.json(responseFormatDict);
                                                                logger.log('info', responseFormatDict);
                                                                logger.log('info', err);
                                                            }
                                                        }
                                                        catch (e) {
                                                            catchFormat(e, res);
                                                        }
                                                    });
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "id is required";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    logger.log('info', err);
                                                }
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "mailID is required";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                res.json(responseFormatDict);    //response
                                                console.log('Error during record insertion : ' + err);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "mailID field is required";
                                            var err = err;
                                            var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                            res.json(responseFormatDict);    //response
                                            console.log('Error during record insertion : ' + err);
                                            logger.log('info', responseFormatDict);
                                            logger.log('info', ('Error during record insertion : ' + err));
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        var data = null;
        var status = false;
        var message = "Something went wrong";
        var err = e;
        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
        res.json(responseFormatDict);
        logger.log('info', responseFormatDict);
        logger.log('info', err);
    }
}

function ExpenseStatusUpdation(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        if (req.body.expenseID != null) {
                                            if ((req.body.expenseID).length != 0) {
                                                if (req.body.admin != null) {
                                                    if ((req.body.admin).length != 0) {
                                                        if (req.body.managerReferenceID != null) {
                                                            if ((req.body.managerReferenceID).length != 0) {
                                                                if (req.body.employeeName != null) {
                                                                    if ((req.body.employeeName).length != 0) {
                                                                        //var id=mongoose.Types.ObjectId(req.body._id);
                                                                        var expense = mongoose.Types.ObjectId(req.body.expenseID);
                                                                        var cuurDate = Date.now();
                                                                        try {
                                                                            ExpenseLine.countDocuments({ expenseID: expense }, (err, count1) => {
                                                                                try {
                                                                                    var d = count1;
                                                                                    console.log("...." + count1);
                                                                                    logger.log('info', count1);
                                                                                    if (req.body.admin == "false") {
                                                                                        ExpenseLine.countDocuments({ $and: [{ expenseID: expense }, { expenseLineStatusID: 2 }] }, (err, count2) => {
                                                                                            try {
                                                                                                console.log("...." + count2);
                                                                                                logger.log('info', count2);
                                                                                                if (d == count2) {
                                                                                                    var data12 = {
                                                                                                        expenseStatusID: 3,
                                                                                                        approvedDate: cuurDate
                                                                                                    }
                                                                                                    Expense.findOneAndUpdate({ _id: expense }, data12, { new: true }, (err, doc) => {
                                                                                                        try {
                                                                                                            if (!err) {
                                                                                                                try {
                                                                                                                    var data2 = { expenseLineStatusID: 1 }
                                                                                                                    ExpenseLine.updateMany({ expenseID: expense }, data2, { new: true }, (err, docs3) => {        // if there is no error ....
                                                                                                                        try {
                                                                                                                            console.log("docs=>" + JSON.stringify(docs3));
                                                                                                                            logger.log('info', ("docs=>" + JSON.stringify(docs3)));
                                                                                                                            Employee.findOne({ employeeID: doc.employeeID }, (err, docs) => {
                                                                                                                                try {
                                                                                                                                    if (!err) {
                                                                                                                                        if (docs != null) {
                                                                                                                                            var msg = "Hi," + doc.expenseID + "is submitted and is awaiting your approval." + "<br><br><br>" + "Thank you";
                                                                                                                                            var sub2 = "Approved: Expense Reimbursement -" + doc.expenseID;
                                                                                                                                            console.log("msg" + docs.employeeEmail);
                                                                                                                                            logger.log('info', ("msg" + docs.employeeEmail));
                                                                                                                                            var adminGmail = 'Abhishek.Shankar@g10x.com';
                                                                                                                                            var sub1 = "Approved: Expense Reimbursement - " + doc.expenseID;
                                                                                                                                            var msg1 = "Dear " + docs.employeeName + "," + "<br>" + "Your Expense" + doc.expenseID + " is approved and is under review with the Expense Reimbursement team for processing." + "<br><br><br>" + "Regards,"
                                                                                                                                            emailNotification(docs.employeeEmail, msg1, sub1);
                                                                                                                                            emailNotification(adminGmail, msg, sub2);
                                                                                                                                            //console.log('new: ',doc7);
                                                                                                                                            var data = null;
                                                                                                                                            var status = true;
                                                                                                                                            var message = "Status Updated";
                                                                                                                                            var err = null;
                                                                                                                                            var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                                                                                            res.json(responseFormatDict);
                                                                                                                                            logger.log('info', responseFormatDict);
                                                                                                                                        } else {
                                                                                                                                            var data = null;
                                                                                                                                            var status = false;
                                                                                                                                            var message = "employeeID is not existing";
                                                                                                                                            var err = null;
                                                                                                                                            var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                                                                                            res.json(responseFormatDict);
                                                                                                                                            logger.log('info', responseFormatDict);
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                catch (e) {
                                                                                                                                    //catchFormat(e,res);
                                                                                                                                    catchFormat(e, res);
                                                                                                                                }
                                                                                                                            })
                                                                                                                        }
                                                                                                                        catch (e) {
                                                                                                                            catchFormat(e, res);
                                                                                                                        }
                                                                                                                    })
                                                                                                                    //console.log("response"+JSON.str)       // response
                                                                                                                }
                                                                                                                catch (e) {
                                                                                                                    var data = null;
                                                                                                                    var status = true;
                                                                                                                    var message = "tytttt";
                                                                                                                    var err = e;
                                                                                                                    var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                                                                    res.json(responseFormatDict);
                                                                                                                    logger.log('info', responseFormatDict);
                                                                                                                    logger.log('info', err);
                                                                                                                }
                                                                                                            }
                                                                                                            else {                      // if there is error.....
                                                                                                                var data = null;
                                                                                                                var status = false;
                                                                                                                var message = "err";
                                                                                                                var err = err;
                                                                                                                var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                                                                res.json(responseFormatDict);    //response
                                                                                                                console.log('Error during record insertion : ' + err);
                                                                                                                logger.log('info', responseFormatDict);
                                                                                                                logger.log('info', ('Error during record insertion : ' + err));
                                                                                                            }    // find the id and update the data
                                                                                                        }
                                                                                                        catch (e) {
                                                                                                            //catchFormat(e,res);
                                                                                                            var data = null;
                                                                                                            var status = true;
                                                                                                            var message = "ppp";
                                                                                                            var err = e;
                                                                                                            var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                                                            res.json(responseFormatDict);
                                                                                                            logger.log('info', responseFormatDict);
                                                                                                            logger.log('info', err);
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                                else {
                                                                                                    Expense.findOneAndUpdate({ _id: expense }, { expenseStatusID: 4 }, { new: true }, (err, doc) => {
                                                                                                        try {
                                                                                                            if (!err) {                  // if there is no error ....
                                                                                                                Employee.findOne({ employeeID: doc.employeeID }, (err, docs) => {
                                                                                                                    try {
                                                                                                                        if (docs != null) {
                                                                                                                            var sub = "Declined: Expense Reimbursement - " + doc.expenseID;
                                                                                                                            var msg = "Dear " + docs.employeeName + "," + "<br>" + "Your Expense " + doc.expenseID + " is declined by the Manager. Please login to the tool for more details." + "<br><br><br>" + "Regards,";
                                                                                                                            emailNotification(docs.employeeEmail, msg, sub)
                                                                                                                            // console.log('updated succeesfylly: ',doc);
                                                                                                                            var data = null;
                                                                                                                            var status = true;
                                                                                                                            var message = "Status Updated";
                                                                                                                            var err = null;
                                                                                                                            var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                                                                            res.json(responseFormatDict);
                                                                                                                            logger.log('info', responseFormatDict);
                                                                                                                            //logger.log('info',err);
                                                                                                                            //console.log("response"+JSON.str)       // response
                                                                                                                        }
                                                                                                                        else {
                                                                                                                            var data = null;
                                                                                                                            var status = false;
                                                                                                                            var message = "employeeID is not existing";
                                                                                                                            var err = null;
                                                                                                                            var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                                                                            res.json(responseFormatDict);
                                                                                                                            logger.log('info', responseFormatDict);
                                                                                                                            //    logger.log('info',err);
                                                                                                                        }
                                                                                                                    }
                                                                                                                    catch (e) {
                                                                                                                        catchFormat(e, res);
                                                                                                                    }
                                                                                                                })
                                                                                                            }
                                                                                                            else {                      // if there is error.....
                                                                                                                var data = null;
                                                                                                                var status = false;
                                                                                                                var message = "err";
                                                                                                                var err = err;
                                                                                                                var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                                                                res.json(responseFormatDict);    //response
                                                                                                                console.log('Error during record insertion : ' + err);
                                                                                                                logger.log('info', responseFormatDict);
                                                                                                                logger.log('info', ('Error during record insertion : ' + err));
                                                                                                            }    // find the id and update the data
                                                                                                        }
                                                                                                        catch (e) {
                                                                                                            catchFormat(e, res);
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            }
                                                                                            catch (e) {
                                                                                                catchFormat(e, res);
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                    else {
                                                                                        ExpenseLine.countDocuments({ $and: [{ expenseID: expense }, { expenseLineStatusID: 2 }] }, (err, count2) => {
                                                                                            try {
                                                                                                console.log("...." + count2);
                                                                                                logger.log('info', count2);
                                                                                                if (d == count2) {
                                                                                                    var data12 = {
                                                                                                        expenseStatusID: 5,
                                                                                                        approvedDate: cuurDate
                                                                                                    }
                                                                                                    Expense.findOneAndUpdate({ _id: expense }, data12, { new: true }, (err, doc) => {
                                                                                                        try {
                                                                                                            if (!err) {                  // if there is no error ....
                                                                                                                Employee.findOne({ employeeID: doc.employeeID }, (err, docs) => {
                                                                                                                    try {
                                                                                                                        if (docs != null) {
                                                                                                                            var msg = "Dear " + docs.employeeName + ", <br>Your Expense " + doc.expenseID + " is accepted by the Expense Reimbursement team. Please log into to the tool for more details.<br><br><br> Regards,"
                                                                                                                            var sub = "Approved: Expense Reimbursement - " + doc.expenseID;
                                                                                                                            emailNotification(docs.employeeEmail, msg, sub)
                                                                                                                            //console.log('updated succeesfylly: ',doc);
                                                                                                                            var data = null;
                                                                                                                            var status = true;
                                                                                                                            var message = "Status Updated";
                                                                                                                            var err = null;
                                                                                                                            var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                                                                            res.json(responseFormatDict);
                                                                                                                            logger.log('info', responseFormatDict);
                                                                                                                        }
                                                                                                                        else {
                                                                                                                            var data = null;
                                                                                                                            var status = false;
                                                                                                                            var message = "employeeID is not existing";
                                                                                                                            var err = null;
                                                                                                                            var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                                                                            res.json(responseFormatDict);
                                                                                                                            logger.log('info', responseFormatDict);
                                                                                                                        }
                                                                                                                    }
                                                                                                                    catch (e) {
                                                                                                                        catchFormat(e, res);
                                                                                                                    }
                                                                                                                })
                                                                                                                //console.log("response"+JSON.str)       // response
                                                                                                            }
                                                                                                            else {                      // if there is error.....
                                                                                                                var data = null;
                                                                                                                var status = false;
                                                                                                                var message = "err";
                                                                                                                var err = err;
                                                                                                                var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                                                                res.json(responseFormatDict);    //response
                                                                                                                console.log('Error during record insertion : ' + err);
                                                                                                                logger.log('info', ('Error during record insertion : ' + err));
                                                                                                                logger.log('info', responseFormatDict);
                                                                                                            }    // find the id and update the data
                                                                                                        }
                                                                                                        catch (e) {
                                                                                                            catchFormat(e, res);
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                                else {
                                                                                                    Expense.findOneAndUpdate({ _id: expense }, { expenseStatusID: 6 }, { new: true }, (err, doc) => {
                                                                                                        try {
                                                                                                            if (!err) {                  // if there is no error ....
                                                                                                                Manager.findOne({ _id: doc.managerReferenceID }, (err, docs4) => {
                                                                                                                    try {
                                                                                                                        if (docs4 != null) {
                                                                                                                            Employee.findOne({ employeeID: doc.employeeID }, (err, docs) => {
                                                                                                                                try {
                                                                                                                                    if (docs != null) {
                                                                                                                                        //console.log("mailid"+docs4.emailID);
                                                                                                                                        var msg1 = "Dear " + docs.employeeName + "," + "<br>" + "Your Expense " + doc.expenseID + "is declined by the Expense Reimbursement team. Please login to the tool for more details." + "<br><br><br>" + "Regards,";
                                                                                                                                        var sub1 = "Declined: Expense Reimbursement - " + doc.expenseID;
                                                                                                                                        emailNotification(docs.employeeEmail, msg1, sub1)
                                                                                                                                        var msg2 = "Dear " + docs4.firstName + " " + docs4.lastName + ", <br>" + doc.expenseID + " approved by you is declined by the Expense Reimbursement team. Please login to the tool for more details." + "<br><br><br>" + "Regards,";
                                                                                                                                        var sub2 = "Declined: Expense Reimbursement - " + doc.expenseID;
                                                                                                                                        emailNotification(docs4.emailID, msg2, sub2)
                                                                                                                                        //console.log('updated succeesfylly: ',doc);
                                                                                                                                        var data = null;
                                                                                                                                        var status = true;
                                                                                                                                        var message = "Status Updated";
                                                                                                                                        var err = null;
                                                                                                                                        var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                                                                                        res.json(responseFormatDict);
                                                                                                                                        logger.log('info', responseFormatDict);
                                                                                                                                    }
                                                                                                                                    else {
                                                                                                                                        var data = null;
                                                                                                                                        var status = false;
                                                                                                                                        var message = "employeeID is not existing";
                                                                                                                                        var err = null;
                                                                                                                                        var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                                                                                        res.json(responseFormatDict);
                                                                                                                                        logger.log('info', responseFormatDict);
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                catch (e) {
                                                                                                                                    catchFormat(e, res);
                                                                                                                                }
                                                                                                                            })
                                                                                                                        }
                                                                                                                        else {
                                                                                                                            var data = null;
                                                                                                                            var status = false;
                                                                                                                            var message = "Manger is invalid";
                                                                                                                            var err = null;
                                                                                                                            var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                                                                            res.json(responseFormatDict);
                                                                                                                            logger.log('info', responseFormatDict);
                                                                                                                        }
                                                                                                                    }
                                                                                                                    catch (e) {
                                                                                                                        catchFormat(e, res);
                                                                                                                    }
                                                                                                                })
                                                                                                                //console.log("response"+JSON.str)       // response
                                                                                                            }
                                                                                                            else {                      // if there is error.....
                                                                                                                var data = null;
                                                                                                                var status = false;
                                                                                                                var message = "err";
                                                                                                                var err = err;
                                                                                                                var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                                                                res.json(responseFormatDict);    //response
                                                                                                                console.log('Error during record insertion : ' + err);
                                                                                                                logger.log('info', ('Error during record insertion : ' + err));
                                                                                                                logger.log('info', responseFormatDict);
                                                                                                            }    // find the id and update the data
                                                                                                        }
                                                                                                        catch (e) {
                                                                                                            catchFormat(e, res);
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            }
                                                                                            catch (e) {
                                                                                                catchFormat(e, res);
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                } catch (e) {
                                                                                    catchFormat(e, res);
                                                                                }
                                                                            });
                                                                        }
                                                                        catch (e) {
                                                                            catchFormat(e, res);
                                                                        }
                                                                    }
                                                                    else {
                                                                        var data = null;
                                                                        var status = false;
                                                                        var message = "employeeName is required";
                                                                        var err = err;
                                                                        var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                        res.json(responseFormatDict);    //response
                                                                        console.log('Error during record insertion : ' + err);
                                                                        logger.log('info', ('Error during record insertion : ' + err));
                                                                        logger.log('info', responseFormatDict);
                                                                    }
                                                                }
                                                                else {
                                                                    var data = null;
                                                                    var status = false;
                                                                    var message = "employeeName is required";
                                                                    var err = err;
                                                                    var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                    res.json(responseFormatDict);    //response
                                                                    console.log('Error during record insertion : ' + err);
                                                                    logger.log('info', ('Error during record insertion : ' + err));
                                                                    logger.log('info', responseFormatDict);
                                                                }
                                                            }
                                                            else {
                                                                var data = null;
                                                                var status = false;
                                                                var message = "managerRefernceID is required";
                                                                var err = err;
                                                                var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                res.json(responseFormatDict);    //response
                                                                console.log('Error during record insertion : ' + err);
                                                                logger.log('info', ('Error during record insertion : ' + err));
                                                                logger.log('info', responseFormatDict);
                                                            }
                                                        }
                                                        else {
                                                            var data = null;
                                                            var status = false;
                                                            var message = "managerReferenceID is required";
                                                            var err = err;
                                                            var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                            res.json(responseFormatDict);    //response
                                                            console.log('Error during record insertion : ' + err);
                                                            logger.log('info', ('Error during record insertion : ' + err));
                                                            logger.log('info', responseFormatDict);
                                                        }
                                                    }
                                                    else {
                                                        var data = null;
                                                        var status = false;
                                                        var message = "admin is required";
                                                        var err = err;
                                                        var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                        res.json(responseFormatDict);    //response
                                                        console.log('Error during record insertion : ' + err);
                                                        logger.log('info', ('Error during record insertion : ' + err));
                                                        logger.log('info', responseFormatDict);
                                                    }
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "admin field is required";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                    res.json(responseFormatDict);    //response
                                                    console.log('Error during record insertion : ' + err);
                                                    logger.log('info', ('Error during record insertion : ' + err));
                                                    logger.log('info', responseFormatDict);
                                                }
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "expenseID is required";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                res.json(responseFormatDict);    //response
                                                console.log('Error during record insertion : ' + err);
                                                logger.log('info', ('Error during record insertion : ' + err));
                                                logger.log('info', responseFormatDict);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "expenseID is required";
                                            var err = err;
                                            var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                            res.json(responseFormatDict);    //response
                                            console.log('Error during record insertion : ' + err);
                                            logger.log('info', ('Error during record insertion : ' + err));
                                            logger.log('info', responseFormatDict);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        //  logger.log('info',('Error during record insertion : ' + err));
                                        logger.log('info', responseFormatDict);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    //   logger.log('info',('Error during record insertion : ' + err));
                                    logger.log('info', responseFormatDict);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', err);
                                logger.log('info', responseFormatDict);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', err);
                        logger.log('info', responseFormatDict);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', err);
                    logger.log('info', responseFormatDict);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', err);
                logger.log('info', responseFormatDict);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', err);
            logger.log('info', responseFormatDict);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
}

function getCurrency(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        Currency.find({}, (err, docs) => {
                                            try {
                                                if (!err) {
                                                    var data = docs;
                                                    var status = true;
                                                    var message = "Record found";
                                                    var err = null;
                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    //logger.log('info',err);
                                                    logger.log('info', responseFormatDict);
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "err";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', err);
                                                    logger.log('info', responseFormatDict);
                                                }
                                            }
                                            catch (e) {
                                                catchFormat(e, res);
                                            }
                                        });
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        // logger.log('info',err);
                                        logger.log('info', responseFormatDict);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    //logger.log('info',err);
                                    logger.log('info', responseFormatDict);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', err);
                                logger.log('info', responseFormatDict);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', err);
                        logger.log('info', responseFormatDict);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', err);
                    logger.log('info', responseFormatDict);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', err);
                logger.log('info', responseFormatDict);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', err);
            logger.log('info', responseFormatDict);
        }
    }
    catch {
        catchFormat(e, res);
    }
}

function signUp(req, res) {
    try {
        // var OTP;
        if ((req.body.employeeEmail) != null) {
            if ((req.body.employeeEmail).length != 0) {
                if ((req.body.password) != null) {
                    if ((req.body.password).length != 0) {
                        var email = req.body.employeeEmail.toLowerCase();
                        Employee.findOne({ employeeEmail: email }, (err, docs) => {
                            try {
                                if (!err) {
                                    if (docs != null) {
                                        if (docs.verifyStatus == "new") {
                                            var OTP = randomstring.generate({
                                                length: 6,
                                                charset: 'numeric'
                                            });;
                                            var key = 'THisisMyKeyTHisM';
                                            var mykeydec1 = crypto.createCipheriv('aes-128-ecb', key, '');
                                            mykeydec1.setAutoPadding(true);
                                            var mystrdec1 = mykeydec1.update(req.body.password, 'utf8', 'base64');
                                            mystrdec1 += mykeydec1.final("base64");
                                            console.log("encriyt" + mystrdec1);
                                            //console.log("dfghjdfghfghjkldfgh"+OTP);
                                            var msg = "Dear " + docs.employeeName + "," + "<br>Your one-time password for ERT registration is " + OTP + "<br><br>Regards,";
                                            var data1 = {
                                                verificationCode: OTP,
                                                verifyStatus: "pending",
                                                password: mystrdec1
                                            };
                                            console.log("dfghjdfghfghjkldfgh" + OTP);
                                            logger.log('info', OTP);
                                            emailNotification(email, msg, "Verification Code: Expense Reimbursement - OTP");
                                            Employee.findOneAndUpdate({ employeeEmail: email }, data1, { useFindAndModify: false }, (err, docs) => {
                                                try {
                                                    if (!err) {
                                                        console.log("dfghjdfghfghjkldfgh" + OTP);
                                                        logger.log('info', OTP);
                                                        //console.log("dfghj"+docs)
                                                        var now = new Date();
                                                        var data = { verifyStatus: docs.verifyStatus, time: now };
                                                        var status = true;
                                                        var message = "Record found";
                                                        var err = null;
                                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                        res.json(responseFormatDict);
                                                        logger.log('info', responseFormatDict);
                                                    }
                                                    else {
                                                        var data = null;
                                                        var status = false;
                                                        var message = "err";
                                                        var err = err;
                                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                        res.json(responseFormatDict);
                                                        logger.log('info', responseFormatDict);
                                                        logger.log('info', err);
                                                    }
                                                }
                                                catch (e) {
                                                    catchFormat(e, res);
                                                }
                                            })
                                        }
                                        else {
                                            if (docs.verifyStatus == "pending") {
                                                var data = { verifyStatus: docs.verifyStatus };
                                                var status = false;
                                                var message = "pending";
                                                var err = null;
                                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                //   logger.log('info',err);
                                            }
                                            else {
                                                var data = { verifyStatus: docs.verifyStatus };
                                                var status = false;
                                                var message = "verified";
                                                var err = null;
                                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                // logger.log('info',err);
                                            }
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Record not found";
                                        var err = null;
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "err";
                                    var err = err;
                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    logger.log('info', err);
                                }
                            }
                            catch (e) {
                                catchFormat(e, res);
                            }
                        });
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "password is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "password is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeEmail is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeEmail is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch {
        catchFormat(e, res);
    }
}

function resendOTP(req, res) {
    try {
        // var OTP;
        if ((req.body.employeeEmail) != null) {
            if ((req.body.employeeEmail).length != 0) {
                var email = req.body.employeeEmail.toLowerCase();
                Employee.findOne({ employeeEmail: email }, (err, docs) => {
                    try {
                        if (!err) {
                            if (docs != null) {
                                if (docs.verifyStatus == "pending" || docs.verifyStatus == "verified") {
                                    var OTP = randomstring.generate({
                                        length: 6,
                                        charset: 'numeric'
                                    });
                                    //console.log("dfghjdfghfghjkldfgh"+OTP);
                                    var msg = "Dear " + docs.employeeName + "," + "<br>" + "Your one-time password for ERT registration is " + OTP + "<br>" + "Regards,";
                                    var data1 = {
                                        verificationCode: OTP,
                                        verifyStatus: "pending",
                                    };
                                    console.log("dfghjdfghfghjkldfgh" + OTP);
                                    logger.log('info', OTP);
                                    emailNotification(email, msg, "Verification Code: Expense Reimbursement - OTP");
                                    // sendOtp.send("7012042063", "9447443009", OTP, callback);
                                    //  sendOTP('9447443009');
                                    Employee.findOneAndUpdate({ employeeEmail: email }, data1, { useFindAndModify: false }, (err, docs) => {
                                        try {
                                            if (!err) {
                                                console.log("dfghjdfghfghjkldfgh" + OTP);
                                                //  return docs;
                                                //console.log("dfghj"+docs)
                                                var now = new Date();
                                                var data = { verifyStatus: docs.verifyStatus, time: now };
                                                var status = true;
                                                var message = "Record found";
                                                var err = null;
                                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "err";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                            }
                                        }
                                        catch (e) {
                                            catchFormat(e, res);
                                        }
                                    })
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Not registered";
                                    var err = null;
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "Record not found";
                                var err = null;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                            }
                        }
                        else {
                            var data = null;
                            var status = false;
                            var message = "err";
                            var err = err;
                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                            res.json(responseFormatDict);
                            logger.log('info', responseFormatDict);
                            logger.log('info', err);
                        }
                    }
                    catch (e) {
                        catchFormat(e, res);
                    }
                });
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeEmail is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeEmail is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch {
        catchFormat(e, res);
    }
}

function verifyOTP(req, res) {
    try {
        var email = req.body.employeeEmail.toLowerCase();
        if ((req.body.employeeEmail) != null) {
            if ((req.body.employeeEmail).length != 0) {
                if ((req.body.verificationCode) != null) {
                    if ((req.body.verificationCode).length != 0) {
                        if ((req.body.time) != null) {
                            if ((req.body.time).length != 0) {
                                Employee.findOne({ employeeEmail: email }, (err, docs) => {
                                    try {
                                        if (!err) {
                                            if (docs != null) {
                                                function DateFormat(date) {
                                                    console.log("tyhj" + date);
                                                    var days = date.getDate();
                                                    var year = date.getFullYear();
                                                    var month = (date.getMonth() + 1);
                                                    var hours = date.getHours();
                                                    console.log("dfgh" + hours);
                                                    var minutes = date.getMinutes();
                                                    minutes = minutes < 10 ? '0' + minutes : minutes;
                                                    var strTime = hours + ':' + minutes;
                                                    // console.log("rt"+strTime)
                                                    return minutes;
                                                }
                                                var date = new Date(req.body.time);
                                                var date1 = new Date();
                                                console.log("req" + req.body.time)
                                                logger.log('info', ("req" + req.body.time));
                                                var actualMinutes = DateFormat(date1);
                                                function AddMinutesToDate(date, minutes) {
                                                    return new Date(date.getTime() + minutes * 60000);
                                                }
                                                var next = AddMinutesToDate(date, 5);
                                                var expireyMinutes = DateFormat(next);
                                                var expireyHours = next.getHours();
                                                // if(expireyT)
                                                var actualHours = date1.getHours();
                                                console.log("actualhours" + actualHours);
                                                console.log("actualminutes" + actualMinutes);
                                                console.log("hours" + expireyHours);
                                                console.log("minutes" + expireyMinutes);
                                                //if(actualHours<=expireyHours&&actualMinutes<=expireyMinutes){
                                                if ((docs.verificationCode) == (req.body.verificationCode)) {
                                                    var msg = "Successfully loggedIn in ERT";
                                                    console.log(msg)
                                                    logger.log('info', msg);
                                                    // console.log("dfghjdfghfghjkldfgh");
                                                    var data4 = {
                                                        verifyStatus: "verified",
                                                        verificationCode: ""
                                                    };
                                                    //  emailNotification(email,msg,"ERT-SIGNIN");
                                                    Employee.findOneAndUpdate({ employeeEmail: email }, data4, { useFindAndModify: false }, (err, docs) => {
                                                        //console.log("dfghjdfghfghjkldfgh"+OTP);
                                                        return docs;
                                                        //console.log("dfghj"+docs)
                                                    }).then(function (docs) {
                                                        console.log("nijuu" + docs);
                                                        logger.log('info', docs);
                                                    })
                                                    var data = null;
                                                    var status = true;
                                                    var message = "Record found";
                                                    var err = null;
                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "Invalid OTP";
                                                    var err = null;
                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                }
                                                /*  }
                                                  else{
                                                      var data=null;
                                                  var status=false;
                                                  var message="OTP expired";
                                                  var err=null;
                                                 var responseFormatDict= responseFormat(data,message,status,err);    //calling the function responseFormat()
                                                  res.json(responseFormatDict);
                                                  logger.log('info',responseFormatDict);
                                                  }*/
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "Record not found";
                                                var err = null;
                                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "err";
                                            var err = err;
                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                            logger.log('info', err);
                                        }
                                    }
                                    catch (e) {
                                        catchFormat(e, res);
                                    }
                                });
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "time is required";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        }
                        else {
                            var data = null;
                            var status = false;
                            var message = "time is required";
                            var err = err;
                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                            res.json(responseFormatDict);
                            logger.log('info', responseFormatDict);
                            logger.log('info', err);
                        }
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "verificationCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "verificationCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "employeeEmail is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "employeeEmail is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch {
        catchFormat(e, res);
    }
}

function getExpenseType(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        ExpenseType.find({}, (err, docs) => {
                                            try {
                                                if (!err) {
                                                    var data = docs;
                                                    var status = true;
                                                    var message = "Record found";
                                                    var err = null;
                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    // logger.log('info',err);
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "err";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    logger.log('info', err);
                                                }
                                            }
                                            catch (e) {
                                                catchFormat(e, res);
                                            }
                                        });
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //  logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    //        logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
}

function InProgressUpdation(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        if ((typeof (req.body.admin) != 'undefined') || (req.body.admin != null)) {
                                            if ((req.body.admin).length != 0) {
                                                if ((typeof (req.body._id) != 'undefined') || (req.body._id != null)) {
                                                    if ((req.body._id).length != 0) {
                                                        if (req.body.admin == "false") {
                                                            ExpenseStatus.findOne({ expenseStatus: "InProgressWithManager" }, { _id: 0 }, (err, doc) => {      // find the id and update the data
                                                                try {
                                                                    if (!err) {
                                                                        // console.log("rrrr"+doc);
                                                                        Expense.findOneAndUpdate({ $or: [{ $and: [{ _id: req.body._id }, { expenseStatusID: 2 }] }, { $and: [{ _id: req.body._id }, { expenseStatusID: 9 }] }] }, { expenseStatusID: doc.ID }, { new: true }, (err, docs) => {      // find the id and update the data
                                                                            try {
                                                                                if (!err) {
                                                                                    if (docs != null) {
                                                                                        var data = docs;
                                                                                        var status = true;
                                                                                        var message = "Status updated";
                                                                                        var err = null;
                                                                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                        res.json(responseFormatDict);
                                                                                        logger.log('info', responseFormatDict);
                                                                                        // logger.log('info',err);
                                                                                    }
                                                                                    else {
                                                                                        var data = null;
                                                                                        var status = true;
                                                                                        var message = "Record not found";
                                                                                        var err = err;
                                                                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                                        res.json(responseFormatDict);
                                                                                        logger.log('info', responseFormatDict);
                                                                                        logger.log('info', err);
                                                                                    }
                                                                                }
                                                                                else {
                                                                                    var data = null;
                                                                                    var status = false;
                                                                                    var message = "err";
                                                                                    var err = err;
                                                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                                    res.json(responseFormatDict);
                                                                                    logger.log('info', responseFormatDict);
                                                                                    logger.log('info', err);
                                                                                }
                                                                            }
                                                                            catch (e) {
                                                                                catchFormat(e, res);
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                                catch (e) {
                                                                    catchFormat(e, res);
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            ExpenseStatus.findOne({ expenseStatus: "InprogressWithAdmin" }, { _id: 0 }, { new: true }, (err, doc) => {      // find the id and update the data
                                                                try {
                                                                    if (!err) {
                                                                        var dat = doc.ID;
                                                                        //console.log("..."+dat);
                                                                        Expense.findOneAndUpdate({ $and: [{ _id: req.body._id }, { expenseStatusID: 3 }] }, { expenseStatusID: doc.ID }, { new: true }, (err, docs) => {      // find the id and update the data
                                                                            try {
                                                                                if (!err) {
                                                                                    if (docs != null) {
                                                                                        var data = docs;
                                                                                        var status = true;
                                                                                        var message = "Status updated";
                                                                                        var err = null;
                                                                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                                                        res.json(responseFormatDict);
                                                                                        logger.log('info', responseFormatDict);
                                                                                        //logger.log('info',err);
                                                                                    }
                                                                                    else {
                                                                                        var data = null;
                                                                                        var status = true;
                                                                                        var message = "Record not found";
                                                                                        var err = err;
                                                                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                                        res.json(responseFormatDict);
                                                                                        logger.log('info', responseFormatDict);
                                                                                        logger.log('info', err);
                                                                                    }
                                                                                }
                                                                                else {
                                                                                    var data = null;
                                                                                    var status = false;
                                                                                    var message = "err";
                                                                                    var err = err;
                                                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                                    res.json(responseFormatDict);
                                                                                    logger.log('info', responseFormatDict);
                                                                                    logger.log('info', err);
                                                                                }
                                                                            }
                                                                            catch (e) {
                                                                                catchFormat(e, res);
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                                catch (e) {
                                                                    catchFormat(e, res);
                                                                }
                                                            });
                                                        }
                                                    }
                                                    else {
                                                        var data = null;
                                                        var status = false;
                                                        var message = "_id is required";
                                                        var err = err;
                                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                        res.json(responseFormatDict);
                                                        logger.log('info', responseFormatDict);
                                                        logger.log('info', err);
                                                    }
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "_id is required";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    logger.log('info', err);
                                                }
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "adminfield is required";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "adminfield is required";
                                            var err = err;
                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                            logger.log('info', err);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        // logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    // logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
}

function getManagerReferenceID(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        if ((typeof (req.body.employeeEmail) != 'undefined') || (req.body.employeeEmail != null)) {
                                            if ((req.body.employeeEmail).length != 0) {
                                                try {
                                                    // var data1=docs;
                                                    //console.log("rr"+docs);
                                                    Employee.findOne({ employeeEmail: req.body.employeeEmail }, (err, docs) => {
                                                        try {
                                                            if (!err) {
                                                                //console.log("trdhfgh"+docs.managerReferenceID);
                                                                if (docs != null) {
                                                                    var data = docs.managerReferenceID;
                                                                    var status = true;
                                                                    var message = "Record found";
                                                                    var err = err;
                                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                    res.json(responseFormatDict);
                                                                    logger.log('info', responseFormatDict);
                                                                    logger.log('info', err);
                                                                }
                                                                //console.log("lst"+data1);
                                                                else {
                                                                    var data = null;
                                                                    var status = false;
                                                                    var message = "Record not found";
                                                                    var err = err;
                                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                    res.json(responseFormatDict);
                                                                    logger.log('info', responseFormatDict);
                                                                    logger.log('info', err);
                                                                }
                                                            }
                                                            else {
                                                                var data = null;
                                                                var status = false;
                                                                var message = "err";
                                                                var err = err;
                                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                                res.json(responseFormatDict);
                                                                logger.log('info', responseFormatDict);
                                                                logger.log('info', err);
                                                            }
                                                        }
                                                        catch (e) {
                                                            catchFormat(e, res);
                                                        }
                                                    })
                                                }
                                                catch (e) {
                                                    catchFormat(e, res);
                                                }
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "employeeEmail is required";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "employeeEmail is required";
                                            var err = err;
                                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                            logger.log('info', err);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        // logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    //   logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
}

function getCostcenter(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        CostCenter.find({}, (err, docs) => {
                                            try {
                                                if (!err) {
                                                    var data = docs;
                                                    var status = true;
                                                    var message = "Record found";
                                                    var err = "null";
                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    //logger.log('info',err);
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "err";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    logger.log('info', err);
                                                }
                                            }
                                            catch (e) {
                                                catchFormat(e, res);
                                            }
                                        });
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //   logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    //    logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
}

function getLocation(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        Location.find({}, (err, docs) => {
                                            try {
                                                if (!err) {
                                                    var data = docs;
                                                    var status = true;
                                                    var message = "Record found";
                                                    var err = "null";
                                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    //logger.log('info',err);
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "err";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    logger.log('info', err);
                                                }
                                            }
                                            catch (e) {
                                                catchFormat(e, res);
                                            }
                                        });
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //   logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    // logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
}

function AcceptOrDecline(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        if (req.body._id != null) {
                                            if ((req.body._id).length != 0) {
                                                if (req.body.status != null) {
                                                    if ((req.body.status).length != 0) {
                                                        if (req.body.status == "Decline") {
                                                            ExpenseLine.findOneAndUpdate({ _id: req.body._id }, { expenseLineStatusID: 3 }, (err, docs) => {
                                                                try {
                                                                    if (!err) {                  // if there is no error ....
                                                                        //console.log('updated succeesfylly: ',doc);
                                                                        var data = null;
                                                                        var status = true;
                                                                        var message = "Status Updated";
                                                                        var err = null;
                                                                        var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                        res.json(responseFormatDict);
                                                                        logger.log('info', responseFormatDict);
                                                                        //logger.log('info',err);
                                                                        //console.log("response"+JSON.str)       // response
                                                                    }
                                                                    else {                      // if there is error.....
                                                                        var data = null;
                                                                        var status = false;
                                                                        var message = "err";
                                                                        var err = err;
                                                                        var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                        res.json(responseFormatDict);    //response
                                                                        logger.log('info', responseFormatDict);
                                                                        //logger.log('info',err);
                                                                        //console.log('Error during record insertion : ' + err);
                                                                    }    // find the id and update the data
                                                                }
                                                                catch (e) {
                                                                    catchFormat(e, res);
                                                                }
                                                            })
                                                        }
                                                        else {
                                                            ExpenseLine.findOneAndUpdate({ _id: req.body._id }, { expenseLineStatusID: 2 }, (err, docs) => {
                                                                try {
                                                                    if (!err) {                  // if there is no error ....
                                                                        //console.log('updated succeesfylly: ',doc);
                                                                        var data = null;
                                                                        var status = true;
                                                                        var message = "Status Updated";
                                                                        var err = null;
                                                                        var responseFormatDict = responseFormat(data, message, status, err);   //calling function responseFormat()
                                                                        res.json(responseFormatDict);
                                                                        logger.log('info', responseFormatDict);
                                                                        // logger.log('info',err);
                                                                        //console.log("response"+JSON.str)       // response
                                                                    }
                                                                    else {                      // if there is error.....
                                                                        var data = null;
                                                                        var status = false;
                                                                        var message = "err";
                                                                        var err = err;
                                                                        var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                                        res.json(responseFormatDict);    //response
                                                                        logger.log('info', responseFormatDict);
                                                                        logger.log('info', err);
                                                                        // console.log('Error during record insertion : ' + err);
                                                                    }    // find the id and update the data
                                                                }
                                                                catch (e) {
                                                                    catchFormat(e, res);
                                                                }
                                                            })
                                                        }
                                                    }
                                                    else {
                                                        var data = null;
                                                        var status = false;
                                                        var message = "status field is required";
                                                        var err = err;
                                                        var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                        res.json(responseFormatDict);    //response
                                                        logger.log('info', responseFormatDict);
                                                        logger.log('info', err);
                                                        // console.log('Error during record insertion : ' + err);
                                                    }
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "status field is required";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                    res.json(responseFormatDict);    //response
                                                    logger.log('info', responseFormatDict);
                                                    logger.log('info', err);
                                                    //console.log('Error during record insertion : ' + err);
                                                }
                                            }
                                            else {
                                                var data = null;
                                                var status = false;
                                                var message = "_id is required";
                                                var err = err;
                                                var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                                res.json(responseFormatDict);
                                                logger.log('info', responseFormatDict);
                                                logger.log('info', err);
                                                console.log('Error during record insertion : ' + err);
                                            }
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "_id is required";
                                            var err = err;
                                            var responseFormatDict = responseFormat(data, message, status, err);  // calling function responseFormat()
                                            res.json(responseFormatDict);    //response
                                            console.log('Error during record insertion : ' + err);
                                            logger.log('info', responseFormatDict);
                                            logger.log('info', ('Error during record insertion : ' + err));
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //  logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    //     logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
}
//emailNotification(newdecoded.mail,managerdecoded.userPrincipalName,'Logged in the system','subject');
function emailNotification(receiverEmail, MsgSent, MsgSubject) {
    try {
        Password.findOne({ ID: "1" }, (err, doc) => {
            try {
                console.log("ggfhg" + doc.password);
                var transporter = nodemailer.createTransport({
                    host: "smtp-mail.outlook.com", // hostname
                    //authetication:true,
                    port: 587,
                    secure: false, // port for secure SMTP
                    auth: {
                        user: "NOREPLY-GXP@g10x.com",
                        //pass:"VS6t5Ro8Ac825lmh"
                        pass: doc.password
                    }
                });
                // setup e-mail data, even with unicode symbols
                var mailOptions = {
                    from: 'NOREPLY-GXP@g10x.com', // sender address (who sends)
                    to: receiverEmail, // list of receivers (who receives)
                    subject: MsgSubject, // Subject line
                    html: MsgSent + '<br><b>Expense Reimbursement Team (GXP)</b><br>'
                    //html:MsgSent, // plaintext body
                    // html: '<b>Hello world </b><br> This is the first email sent with Nodemailer in Node.js' // html body
                    /*html:'<img style="width:50px;" src="cid:unique@kreata.ee">',
                        attachments:[{
                          filename :'signature.png',
                          path:'./controllers/img/signature.png',
                          cid: 'unique@kreata.ee'
                        }]   */
                };
                transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        return console.log(error);
                    }
                    else {
                        console.log('Message sent: ' + info.response);
                    }
                });
            }
            catch (e) {
                catchFormat(e, res);
            }
        });
    }
    catch (e) {
        catchFormat(e, res);
    }
}

function logout(req, res) {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        Employee.findOne({ employeeID: req.body.employeeID }, (err, doc) => {
                            if (!err) {
                                if (doc != null) {
                                    if ((req.body.SecurityCode) == (doc.SecurityCode)) {
                                        var data1 = {
                                            SecurityCode: ""
                                        }
                                        Employee.findOneAndUpdate({ employeeID: req.body.employeeID }, data1, (err, docs) => {
                                            try {
                                                if (!err) {
                                                    if (docs != null) {
                                                        var data = null;
                                                        var status = true;
                                                        var message = "Logout successfully";
                                                        var err = null;
                                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                        res.json(responseFormatDict);
                                                        logger.log('info', responseFormatDict);
                                                        //  logger.log('info',err);
                                                    }
                                                    else {
                                                        var data = null;
                                                        var status = false;
                                                        var message = "Record notnfound";
                                                        var err = null;
                                                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                        res.json(responseFormatDict);
                                                        logger.log('info', responseFormatDict);
                                                        //   logger.log('info',err);
                                                    }
                                                }
                                                else {
                                                    var data = null;
                                                    var status = false;
                                                    var message = "err";
                                                    var err = err;
                                                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                                                    res.json(responseFormatDict);
                                                    logger.log('info', responseFormatDict);
                                                    logger.log('info', err);
                                                }
                                            }
                                            catch (e) {
                                                catchFormat(e, res);
                                            }
                                        })
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Access Denied";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                        //             logger.log('info',err);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Employee ID is not valid";
                                    var err = "null";
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    //         logger.log('info',err);
                                }
                            }
                            else {
                                var data = null;
                                var status = false;
                                var message = "err";
                                var err = err;
                                var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                res.json(responseFormatDict);
                                logger.log('info', responseFormatDict);
                                logger.log('info', err);
                            }
                        })
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "SecurityCode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        logger.log('info', err);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    logger.log('info', err);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
                logger.log('info', err);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
            logger.log('info', err);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
}

function resetPassword(req, res) {
    try {
        var post_data = req.body;
        var userName = post_data.employeeEmail.toLowerCase();
        var newPassword = post_data.password;
        var key = 'THisisMyKeyTHisM';
        var myNewkeydec = crypto.createCipheriv('aes-128-ecb', key, null);
        var myNewstrdec = myNewkeydec.update(newPassword, 'utf8', 'base64');
        myNewstrdec += myNewkeydec.final("base64");
        console.log("dec data", myNewstrdec);
        var data1 = {
            password: myNewstrdec,
            verificationStatus: "verified"
        }
        Employee.findOne({ employeeEmail: userName }, (err, docs) => {
            try {
                if (docs != null) {
                    if (docs.verifyStatus == "verified") {
                        Employee.findOneAndUpdate({ employeeEmail: userName }, data1, { new: true }, (err, docs) => {
                            try {
                                //console.log(docs.verificationStatus);
                                if (!err) {
                                    var data = docs;
                                    console.log(docs);
                                    logger.log('info', docs);
                                    return data;
                                }
                            }
                            catch (e) {
                                catchFormat(e, res);
                            }
                        }).then(function (docs2) {
                            setTimeout(function () { }, 2000);
                            var data = null;
                            var status = true;
                            var message = "updated pwd";
                            var err = null;
                            var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                            res.json(responseFormatDict);
                            logger.log('info', responseFormatDict);
                            // logger.log('info',err);
                        })
                    } else {
                        var data = null;
                        var status = true;
                        var message = "user is not verified";
                        var err = null;
                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                        // logger.log('info',err);
                    }
                }
                else {
                    var data = null;
                    var status = true;
                    var message = "user not defined";
                    var err = null;
                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                    //logger.log('info',err);
                }
            }
            catch (e) {
                catchFormat(e, res);
            }
        });
    }
    catch (e) {
        catchFormat(e, res);
    }
}

router.post('/login', (req, resp, next) => {
    try {
        var post_data = req.body;
        var userName = post_data.username.toLowerCase();
        var userPassword = post_data.password;
        console.log("username" + userName)
        if (userName == "" || userPassword == "") {
            var data = null;
            var status = false;
            var message = "Username / password cannot be empty";
            var error = null;
            var callresponse = responseFormat(data, message, status, error);
            resp.send(callresponse);
            logger.log('info', callresponse);
            //                                   logger.log('info',err);
        }
        else {
            console.log("asdsa")
            Employee.findOne({ employeeEmail: userName }, (err, docs) => {
                // console.log(docs)
                try {
                    if (docs != null) {
                        if (docs.verifyStatus == "new") {
                            var data = { verifyStatus: docs.verifyStatus };
                            var status = false;
                            var message = "not registered";
                            var error = null;
                            var callresponse = responseFormat(data, message, status, error);
                            resp.send(callresponse);
                            logger.log('info', callresponse);
                            //              logger.log('info',err);
                        }
                        else {
                            if (docs.verifyStatus == "pending") {
                                var data = { verifyStatus: docs.verifyStatus };
                                var status = false;
                                var message = "not verified";
                                var error = null;
                                var callresponse = responseFormat(data, message, status, error);
                                resp.send(callresponse);
                                logger.log('info', callresponse);
                                //    logger.log('info',err);
                            }
                            else {
                                console.log("1st");
                                try {
                                    var key = 'THisisMyKeyTHisM';
                                    console.log("6");
                                    var mykeydec = crypto.createDecipheriv('aes-128-ecb', key, null);
                                    console.log("1.5", docs.password);
                                    var mystrdec = mykeydec.update(docs.password, 'base64', 'ascii');
                                    mystrdec += mykeydec.final("ascii");
                                    console.log("dec dataf", mystrdec);
                                    var mykeydecG10x = crypto.createDecipheriv('aes-128-ecb', key, null);
                                    //console.log("2ns");
                                    // var mystrdecG10x=mykeydecG10x.update('Qv783iW9VIfAx1a3SdNhNFnRILZ4Qt4c5Zx0/nqUKPU=', 'base64', 'ascii');
                                    //console.log("3");
                                    //   mystrdecG10x+=mykeydecG10x.final("ascii");
                                }
                                catch (e) {
                                    var data = null;
                                    var status = false;
                                    var message = "Something went wrong plzzzzzz";
                                    console.log(message);
                                    logger.log('info', message);
                                    var err = e;
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                    logger.log('info', e);
                                }
                                var key = 'THisisMyKeyTHisM';
                                var mykeydec2 = crypto.createDecipheriv('aes-128-ecb', key, null);
                                var mystrdec2 = mykeydec2.update(req.body.password, 'base64', 'ascii');
                                mystrdec2 += mykeydec2.final("ascii");

                                console.log("qwewwwww")

                                if (mystrdec2 == mystrdec) {
                                    console.log("qwaae")

                                    var securityToken = randomstring.generate({
                                        length: 50,
                                        charset: 'alphanumeric'
                                    });
                                    console.log("qwe")
                                    Employee.findOne({ employeeEmail: userName }, (err, docs) => {
                                        try {
                                            if (docs != null) {
                                                console.log("drfgh" + docs);
                                                logger.log('info', docs);
                                                //  var emp = new EmployeeDetail()
                                                var data8 = {
                                                    SecurityCode: securityToken
                                                }
                                                // emp.managerReferenceID=doc7._id;
                                                console.log("fghj" + securityToken);
                                                console.log("dta8" + JSON.stringify(data8));
                                                Employee.findOneAndUpdate({ employeeEmail: userName }, data8, (err, docs2) => {
                                                    try {
                                                        if (!err) {
                                                            var data = docs2;
                                                            console.log("nivi" + docs2);
                                                            logger.log('info', docs2);
                                                            return data;
                                                        }
                                                    }
                                                    catch (e) {
                                                        catchFormat(e, res);
                                                    }
                                                })
                                                    .then(function (docs2) {
                                                        console.log("rdhtfgyh");
                                                        setTimeout(function () { }, 2000);
                                                        Employee.findOne({ employeeEmail: userName }, { verificationCode: 0, password: 0 }, (err, docs7) => {
                                                            try {
                                                                var data2 = docs7;
                                                                var status = true;
                                                                var message = "logged in";
                                                                var error = null;
                                                                var callresponse = responseFormat(data2, message, status, error);
                                                                resp.send(callresponse);
                                                                logger.log('info', callresponse);
                                                                console.log("5555");
                                                            }
                                                            catch (e) {
                                                                catchFormat(e, res);
                                                            }
                                                        })
                                                    })
                                            }
                                        }
                                        catch (e) {
                                            catchFormat(e, res);
                                        }
                                    });
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "Invalid Password: Try again";
                                    var error = null;
                                    var callresponse = responseFormat(data, message, status, error);
                                    resp.send(callresponse);
                                    logger.log('info', callresponse);
                                }
                            }
                        }
                    } else {
                        var data = null;
                        var status = false;
                        var message = "Invalid Email: Try again";
                        var error = null;
                        var callresponse = responseFormat(data, message, status, error);
                        resp.send(callresponse);
                        logger.log('info', callresponse);
                    }
                }
                catch (e) {
                    console.log("sdaasd")
                    catchFormat(e, resp);
                }
            });
        }
    }
    catch (e) {
        catchFormat(e, resp);
    }
});
app.use(express.static('public'));
function stripquotes(a) {
    if (a.charAt(0) === '"' && a.charAt(a.length - 1) === '"') {
        return a.substr(1, a.length - 2);
    }
    return a;
}
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, __dirname + '/fileupload')
        //console.log(__dirname+'fileupload');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
        //console.log(file.originalname);
    }
})
const upload = multer({ storage: storage });

router.post('/uploadSmallImage', upload.single('photo'), (req, res, next) => {
    try {
        if ((req.body.employeeID) != null) {
            if ((req.body.employeeID).length != 0) {
                if ((req.body.SecurityCode) != null) {
                    if ((req.body.SecurityCode).length != 0) {
                        if (req.file != null) {
                            var empID = stripquotes(req.body.employeeID);
                            var secID = stripquotes(req.body.SecurityCode);
                            Employee.findOne({ employeeID: empID }, (err, doc) => {
                                if (!err) {
                                    if (doc != null) {
                                        if ((secID) == (doc.SecurityCode)) {
                                            const ID = 'AKIA26P6GBEW7ZTCJI4S';
                                            const SECRET = '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';
                                            // The name of the bucket that you have created
                                            const BUCKET_NAME = 'gxxstorage';
                                            const s3 = new AWS.S3({
                                                accessKeyId: ID,
                                                secretAccessKey: SECRET
                                            });
                                            let ts = Date.now();
                                            // timestamp in milliseconds
                                            console.log(ts);
                                            // timestamp in seconds
                                            let namePdf = Math.floor(ts / 1000);
                                            let name = namePdf + '.pdf';
                                            const uploadFile = (fileName) => {
                                                // Read content from the file
                                                const fileContent = fs.readFileSync(fileName);
                                                // Setting up S3 upload parameters
                                                var folder = 'Receipts';
                                                const params = {
                                                    Bucket: BUCKET_NAME,
                                                    //Key: name, // File name you want to save as in S3
                                                    Key: `${folder}/${name}`,
                                                    // ServerSideEncryption: "AES256",
                                                    ContentDisposition: "inline",
                                                    ContentType: 'application/pdf',
                                                    ACL: 'public-read',
                                                    Body: fileContent
                                                };
                                                /* const getUrlFromBucket=(s3Bucket,fileName)=>{
                                                     return 'https://'+s3Bucket.config.params.Bucket+'.s3-'
                                                             + s3Bucket.config.region+'.amazonaws.com/'+fileName
                                                 };
                                                 var u=getUrlFromBucket('gxxstorage',name);
                                                 console.log('The URL ishh', u);*/
                                                // Uploading files to the bucket
                                                s3.upload(params, function (err, data) {
                                                    if (err) {
                                                        throw err;
                                                    }
                                                    // var params = {Bucket: 'gxxstorage', Key: '${folder}/${name}'};
                                                    s3.getSignedUrl('putObject', params, function (err, signedURL) {
                                                        console.log('The URL is', signedURL);
                                                        console.log('The URL is...', signedURL.split("?")[0]);
                                                        /*var s3url = s3.getSignedUrl('getObject', {Key: params.Key});
                                                        console.log('The is', s3url);*/
                                                        const myBucket = 'gxxstorage'
                                                        const myKey = 'NCqjsg0ZDWf3BeZeQIuZNBNqAJI+FEoaGMd/SpSG'
                                                        const signedUrlExpireSeconds = 60 * 5
                                                        const url = s3.getSignedUrl('getObject', {
                                                            Bucket: myBucket,
                                                            Key: myKey,
                                                            Expires: signedUrlExpireSeconds
                                                        })
                                                        console.log(url)
                                                        var data = { url: signedURL.split("?")[0] };
                                                        var status = true;
                                                        var message = "file upload successfully";
                                                        var err = "null";
                                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                                        res.json(responseFormatDict);
                                                        logger.log('info', responseFormatDict);
                                                        // console.log(`File uploaded successfully. ${data.Location}`);
                                                    });
                                                })
                                            };
                                            uploadFile(__dirname + '/fileupload/' + req.file.filename);
                                        }
                                        else {
                                            var data = null;
                                            var status = false;
                                            var message = "Access Denied";
                                            var err = "null";
                                            var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                            res.json(responseFormatDict);
                                            logger.log('info', responseFormatDict);
                                        }
                                    }
                                    else {
                                        var data = null;
                                        var status = false;
                                        var message = "Employee ID is not valid";
                                        var err = "null";
                                        var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                        res.json(responseFormatDict);
                                        logger.log('info', responseFormatDict);
                                    }
                                }
                                else {
                                    var data = null;
                                    var status = false;
                                    var message = "err";
                                    var err = err;
                                    var responseFormatDict = responseFormat(data, message, status, err);    //calling the function responseFormat()
                                    res.json(responseFormatDict);
                                    logger.log('info', responseFormatDict);
                                }
                            })
                        }
                        else {
                            var data = null;
                            var status = false;
                            var message = "file is empty";
                            var err = err;
                            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                            res.json(responseFormatDict);
                            logger.log('info', responseFormatDict);
                        }
                    }
                    else {
                        var data = null;
                        var status = false;
                        var message = "Securitycode is required";
                        var err = err;
                        var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                        res.json(responseFormatDict);
                        logger.log('info', responseFormatDict);
                    }
                }
                else {
                    var data = null;
                    var status = false;
                    var message = "SecurityCode is required";
                    var err = err;
                    var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                    res.json(responseFormatDict);
                    logger.log('info', responseFormatDict);
                }
            }
            else {
                var data = null;
                var status = false;
                var message = "EmployeeID is required";
                var err = err;
                var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
                res.json(responseFormatDict);
                logger.log('info', responseFormatDict);
            }
        }
        else {
            var data = null;
            var status = false;
            var message = "EmployeeID is required";
            var err = err;
            var responseFormatDict = responseFormat(data, message, status, err);     //calling the function responseFormat()
            res.json(responseFormatDict);
            logger.log('info', responseFormatDict);
        }
    }
    catch (e) {
        catchFormat(e, res);
    }
});
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, __dirname + '/image')
        //console.log(__dirname+'fileupload');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
        //console.log(file.originalname);
    }
})
const upload2 = multer({ storage: storage });
module.exports = router;
