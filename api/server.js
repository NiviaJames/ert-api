
require('./models/db');


const express = require('express');
//const path = require('path');

const bodyparser = require('body-parser');


const employeeController = require('./controllers/employeeController');

var app = express();

app.use(bodyparser.urlencoded({
    extended: true
}));
app.use(bodyparser.json());

app.listen(2000, () => {
    console.log('Express server started at port : 2000');
});

app.use('/home', employeeController);