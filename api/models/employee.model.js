const mongoose = require('mongoose');
var ExpenseSchema = new mongoose.Schema({
    employeeID:{
        type:String,
        required:'This feild is required'
    },
    employeeName:{
        type:String,
        required:'This feild is required'
    },
    expenseID: {
        type: String,
    },
    managerReferenceID: {
        type: String,
        required:'This feild is required'
    },
    expensePurpose: {
        type: String,
        required:"this feild is required"
    },
    expenseStatusID: {
        type: String,
    },
    reSubmittedToManager: {
        type: Boolean,
    },
    reSubmittedToAdmin: {
        type: Boolean,
    },
    updatedDate: {
        type: Date,
    },
    deletionStatus:{
        type: Boolean,
    },
    createdDate: {
        type: Date,
        required: 'This field is required.'
    },
    approvedDate: {
        type: Date
    },
    createdBy: {
        type: String,
    },
    updatedBy: {
        type: String,
    }
});

var ExpenseLineSchema = new mongoose.Schema({
    expenseID: {
    },
    expenseType: {
        type:String,
        required:'This feild is required'
    },
    otherExpense: {
        type:String,
    },
    merchant: {
        type: String,
        required:'This feild is required'
    },
    costCenter: {
        type: String,
        required:'This feild is required'
    },
    noOfEmployee: {
        type: Number,
        required:'This feild is required'
    },
    nameOfEmployee: {
        type: String,
        required:'This feild is required'
    },
    location: {
        type: String,
        required:'This feild is required'
    },
    currency: {
        type:String,
        required:'This feild is required'
    },
    amount: {
        type: Number,
        required:'This feild is required'
    },
    comments: {
        type: String
    },
    declineComments: {
        type: String
    },
    expenseLineStatusID: {
        type: String
    },
    receiptID: {
        type: String
    },
    url: {
        type: String
    },
    date: {
        type:Date,
        required:'This feild is required'
    },
    updatedDate: {
        type: Date,
    },
    createdDate: {
        type: Date
    },
    createdBy: {
        type: String
    },
    updatedBy: {
        type: String
    }
});

var ManagerSchema = new mongoose.Schema({
    employeeID:{
        type:String
    },
    managerReferenceID: {
        type: String,
    },
    emailID: {
        type: String,
    },
    firstName: {
        type: String
    },
    lastName: {
        type: String,
    },
    createdDate: {
        type: Date
    },
    updatedDate: {
        type: Date,
    },
    createdBy: {
        type: String
    },
    updatedBy: {
        type: String
    }
});

var ExpenseLineStatusSchema = new mongoose.Schema({
    ID:{
        type:String
    },
    expenseLineStatus: {
        type: String,
    }
});

var CurrencyStatusSchema = new mongoose.Schema({
    ID:{
        type:String
    },
    currency: {
        type: String,
    },
    currencyCode: {
        type: String,
    },
    currencyDescription: {
        type: String,
    }
});

var ExpenseStatusSchema = new mongoose.Schema({
    ID:{
        type:String
    },
    expenseStatus: {
        type: String,
    },
    Description: {
        type: String,
    }
});

var ExpenseTypeSchema = new mongoose.Schema({
    ID:{
        type:Number
    },
    expenseType: {
        type: String,
    }
});

var ExpenseIDSchema = new mongoose.Schema({
    ExpenseID: {
        type: String,
    }
});

var employeeSchema  = new mongoose.Schema({
    employeeID:{
        type:String
    },
    employeeName: {
        type: String,
    },
    employeeRole:{
        type:String
    },
    employeeEmail:{
        type:String
    },
    managerReferenceID:{
        type:String
    },
    verificationCode:{
        type:String
    },
    SecurityCode:{
        type:String
    },
    password:{
        type:String
    },
    verifyStatus:{
        type:String
    },
});

var costCenterSchema = new mongoose.Schema({
    Id:{
        type:Number
    },
    costcenter: {
        type: String,
    }
});

var PasswordSchema = new mongoose.Schema({
    ID:{
        type:String
    },
    email: {
        type: String,
    },
    password: {
        type: String,
    }
});

var LocationSchema = new mongoose.Schema({
    Id:{
        type:Number
    },
    location:{
        type: String,
    }
});

mongoose.model('employee',employeeSchema);
//mongoose.model('EmployeeDetail',EmployeeDetailSchema);
mongoose.model('CostCenter', costCenterSchema);
mongoose.model('Location', LocationSchema);
mongoose.model('Expense', ExpenseSchema);
mongoose.model('Currency', CurrencyStatusSchema);
mongoose.model('ExpenseLineStatus', ExpenseLineStatusSchema);
mongoose.model('ExpenseType', ExpenseTypeSchema);
mongoose.model('ExpenseStatus', ExpenseStatusSchema);
mongoose.model('Manager', ManagerSchema);
mongoose.model('ExpenseLine', ExpenseLineSchema);
mongoose.model('Manager', ManagerSchema);
mongoose.model('ExpenseID', ExpenseIDSchema);
mongoose.model('Password', PasswordSchema);